# Phononics with COMSOL

## Sapper Structure
Small library that calculates the phononic band structures (symmetric modes only) 
for quadratic unitcell with an arbitrary pattern. See the "TestSapperStructure.m"-script 
for an example of its funcionalty. Here, object S of the class SapperStructure is generated 
and different methods of the of the object are applied (e.g. calcukateBandstructure).

Note that COMSOL and COMSOLwithMATLAB is required to execute the Matlab code.
