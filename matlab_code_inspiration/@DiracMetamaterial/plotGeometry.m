function plotGeometry( obj, extended, unitsector, bezPoints, restrictions, material)
%PLOTGEOMETRY plots the geometry 
%   extended:       plots multiple unitcells
%   unitsector:     displays the unitsector if true
%   bezPoints:      displays the bezPoints if true
%   restrictions:   displays the boundarys of the hole
%   material:       displays the [100] and [010] crystal axis

    lw_hex = 2;
    lw_geo = 2;
    lw_unitsector = 2;
    lw_bez = 2;
    lw_material = 2;
    lw_restrictions = 2; 
    
    
    blue = [0    0.4470    0.7410];
    red =  [0.6350    0.0780    0.1840];
    lightblue = [0.3010    0.7450    0.9330];
    gray = 240/256 * ones(1,3);
        gray = 1 * ones(1,3);
    
    materialColor = 50/256 * ones(1,3);
        
    orange = [252, 132, 43]/256;
    
    R60 = [cos(pi/3), -sin(pi/3); sin(pi/3), cos(pi/3)];
    
    hold on; 
    axis equal;
    
   
    
    %plot the geometry
    X = [obj.coordGeo(1,:),obj.coordGeo(1,1)];
    Y = [obj.coordGeo(2,:),obj.coordGeo(2,1)];
    fill(X,Y,gray);
    plot(X,Y,'LineWidth',lw_geo,'Color','black');
    if (extended == true)
        for i=1:6
            V = (R60^i)*[obj.aMech;0]; %displacement vector
            fill(X+V(1),Y+V(2),gray);
            plot(X+V(1),Y+V(2),'LineWidth',lw_hex,'Color','black');
        end
    end
    
    %plot the cavity
    if(~isempty(obj.coordHoles))
        for i=1:size(obj.coordHoles,2)
            phi = linspace(0,2*pi,10);
            X = obj.coordHoles(1,i) + obj.rOpt*sin(phi);
            Y = obj.coordHoles(2,i) + obj.rOpt*cos(phi);
            fill(X,Y,gray);
        end
    end
    if (extended == true)
        for j=1:6
            V = (R60^j)*[obj.aMech;0]; %displacement vector
            if(~isempty(obj.coordHoles))
                for i=1:size(obj.coordHoles,2)
                    phi = linspace(0,2*pi,10);
                    X = V(1)+obj.coordHoles(1,i) + obj.rOpt*sin(phi);
                    Y = V(2)+obj.coordHoles(2,i) + obj.rOpt*cos(phi);
                    fill(X,Y,gray);
                end
            end
        end
    end
    
    
    %plot the hexagon
    X = [obj.coordHex(1,:),obj.coordHex(1,1)];
    Y = [obj.coordHex(2,:),obj.coordHex(2,1)];
    plot(X,Y,'LineWidth',lw_hex,'Color','yellow'); 
    %{
    if (extended == true)
        for i=1:6
            V = (R60^i)*[obj.aMech;0]; %displacement vector
            plot(X+V(1),Y+V(2),'LineWidth',lw_hex,'Color','black');
        end
    end
    %}
    
    %plot the unitsector
    if(unitsector == true)
        X = [obj.coordUnitSector(1,:),obj.coordUnitSector(1,1)];
        Y = [obj.coordUnitSector(2,:),obj.coordUnitSector(2,1)];
        plot(X,Y,'LineWidth',lw_unitsector,'Color','yellow','LineStyle','-');
    end
    
    
    %plot the Bezier points
    if(bezPoints == true)
        for i=1:size(obj.coordBez,2)
            x = obj.coordBez(1,i);
            y = obj.coordBez(2,i);
            w = obj.coordBez(3,i);
            a = obj.coordBez(4,i);
            
            xx = [x+w*cos(a),x];
            yy = [y+w*sin(a),y];
            
            plot(xx,yy,'Marker','.','Color', orange, 'LineWidth',lw_bez);
        end
    end
    
    
    %plot restricting area
    if(restrictions == true)
        X = obj.spacingToCenter*sin(linspace(0,2*pi));
        Y = obj.spacingToCenter*cos(linspace(0,2*pi));
        plot(X,Y,'Color',red,'LineStyle',':','LineWidth',lw_restrictions);   
        
        smallerHex = obj.generateHexagon(obj.aMech-obj.spacingToHex);
        X = [smallerHex(1,:),smallerHex(1,1)];
        Y = [smallerHex(2,:),smallerHex(2,1)];
        plot(X,Y,'Color',red,'LineStyle',':','LineWidth',lw_restrictions);
        
        if( isempty(obj.coordGeoEnlarged)==false )
            X = [obj.coordGeoEnlarged(1,:),obj.coordGeoEnlarged(1,1)];
            Y = [obj.coordGeoEnlarged(2,:),obj.coordGeoEnlarged(2,1)];
            plot(X,Y,'Color',red,'LineStyle',':','LineWidth',lw_restrictions);
        end
    end
    
    %plot the direction of the [100] and [010] direction
    if(material == true)
        if(obj.anisotropic == true)
            v100 = [1;0];
            v010 = [0;1];

            R = [cos(obj.beta), -sin(obj.beta);...
                 sin(obj.beta), cos(obj.beta)];

            v100 = R*v100;
            v010 = R*v010;


            X = [0,0];
            Y = [0,0];
            U = [v100(1),v010(1)]*obj.aMech/8;
            V = [v100(2),v010(2)]*obj.aMech/8;

            quiver(X,Y,U,V,'LineWidth',lw_material,'Color',lightblue,'MaxHeadSize',0.5)
            text(U(1)*1.3,V(1)*1.3,'[100]','FontSize',12,'FontWeight','bold','Color',lightblue,'HorizontalAlignment','center','VerticalAlignment','middle');
            text(U(2)*1.3,V(2)*1.3,'[010]','FontSize',12,'FontWeight','bold','Color',lightblue,'HorizontalAlignment','center','VerticalAlignment','middle');
        end
    end
   
    % other things
    axis(1*[-obj.aMech, obj.aMech , -obj.aMech ,obj.aMech]);
    box on;
    set(gca,'Color',materialColor)
    set(gcf, 'InvertHardcopy', 'off')
    
    %lattice consnat label 
    g=gca;
    X = max(g.XLim);
    Y = max(g.YLim);
    T = ['a=',num2str(round(obj.aMech)),'nm'];
    t = text( X,Y, T);
    t.FontSize = 16;
    t.FontWeight = 'bold';
    t.Color = 'white';
    t.BackgroundColor = 'black';
    t.HorizontalAlignment='right';
    t.VerticalAlignment='top';
    
    %no ticks 
    ax = gca; 
    ax.XTick = [];
    ax.YTick = [];
    
    
    
    
    
    
    


end

