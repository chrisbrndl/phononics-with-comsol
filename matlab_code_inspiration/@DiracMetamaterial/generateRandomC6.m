function obj = generateRandomC6(aMin,aMax,d,n,coordRes,beta,anisotropic,spacingToHex,spacingToCenter,directPathWayFraction,maxMaterial)
%GENEATERANDOMC6V generates a random geometry with a C6 symmetry by unsig
%the following parameters
%   aMin                    minimal lattice constant
%   aMax                    maximum lattice constant
%   d                       DiracMetamaterial.d
%   n                       number of BezierPoints in a unit-sector (30 degree sector)
%   coordRes                DiracMetamaterial.coordRes
%   alpha                   DiracMetamaterial.alpha
%   spacingToHex            DiracMetamaterial.spacingToHex
%   spacingToCenter         DiracMetamaterial.spacingToCenter
%   directPathWayFraction   DiracMetamateria.directPathWayFraction


    %initalize an instance
    obj = DiracMetamaterial;
    obj.d = d; 
    obj.coordRes = coordRes;
    obj.spacingToHex = spacingToHex; 
    obj.spacingToCenter = spacingToCenter;
    obj.directPathWayFraction = directPathWayFraction;
    obj.symmetry = 'C6';
    obj.beta = beta;
    obj.anisotropic = anisotropic;
    obj.maxMaterial = maxMaterial;
    obj.aMechLimits = [aMin,aMax];
    
    
    %get random lattice constant
	aMech = aMin + (aMax-aMin).*rand(1,1);    
    obj.aMech = aMech;
    
    
    
    %build the hexagon (boundary)
    coordHex = obj.generateHexagon(aMech);
    obj.coordHex = coordHex;
    
    
    %unit sector 
    unitsector =   [0, aMech/2, aMech/2;...
                        0, -aMech/2/sqrt(3), aMech/2/sqrt(3)];
    obj.coordUnitSector = unitsector;
                    
                    
    ok = 0;
    while (ok==0)
        
        %get n random Bezier-Points
        %x and y should be within a sector of -30 and 30 degree (measured from the x-axis)
        %alpha can be anythin between 0 an 2*pi
        %the weight is set to be someting between 0 and aMech/4
        inside = 0;
        while(inside == 0)
            x = aMech/2 * rand(1,n);
            y = aMech/2/sqrt(3) * 2*(rand(1,n)-0.5);
            in = inpolygon(x,y,unitsector(1,:),unitsector(2,:));
            inside = min(in);
        end
        
        alpha = 2*pi * rand(1,n);

        weight = aMech/2 * rand(1,n);

        
        
        %generate the coordBez attribute 
        coordBez = [x; y; weight; alpha];
        obj.coordBez= coordBez;
        coordBezComplete = obj.dublicateC6( coordBez );
        obj.coordBezComplete = coordBezComplete; 
        
        %generate the full Bezier polygon
        coordGeo = [];
        for i=1:size(coordBezComplete,2)
            
            j=i+1;
            if(i==size(coordBezComplete,2)) 
                j = 1;
            end
            
            cubicBezierCurve = obj.generateCubicBezierCurve(coordBezComplete(:,i),coordBezComplete(:,j),coordRes);
            coordGeo = [coordGeo, cubicBezierCurve];
        end
        
        
        obj.coordGeo = coordGeo;
        obj.areaHex = polyarea(obj.coordHex(1,:),obj.coordHex(2,:));
        obj.areaGeo = polyarea(obj.coordGeo(1,:),obj.coordGeo(2,:));
        
        %check spacingToCenter
        ok1 = obj.checkSpacingToCenter();
        
        %check spacingToHex 
        ok2 = obj.checkSpacingToHex();
        
        %check intersections
        ok3 = obj.checkIntersections();
        
        %check pathway fractions
        ok4 = obj.checkDirectPathWayFraction();
        
        %check maximum material area
        ok5 = obj.checkMaximumMaterial();
        
        ok = ok1*ok2*ok3*ok4*ok5;
        
        
    end    
        
    
        
        


        %to check
        %{
        figure(3)
        clf;
        hold on;
        axis equal;
        
        plot([unitsector(1,:),unitsector(1,1)],[unitsector(2,:),unitsector(2,1)]);
        
        plot(coordBezComplete(1,:),coordBezComplete(2,:),'Marker','o','LineStyle','none');
        plot(coordBez(1,:),coordBez(2,:),'Marker','o','LineStyle','none','Color','blue');
        plot([coordHex(1,:),coordHex(1,1)],[coordHex(2,:),coordHex(2,1)])
        %plot(coordGeo(1,:),coordGeo(2,:),'Marker','.');
    
        
        
        %plot(spacingToCenter*sin(linspace(0,2*pi)), spacingToCenter*cos(linspace(0,2*pi)));
        
        %smallerHex = obj.generateHexagon(aMech-spacingToHex);
        %plot(smallerHex(1,:),smallerHex(2,:))
        %}
    
end

