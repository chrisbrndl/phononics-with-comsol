function model = calculateBandStructure( obj, model )
%CALCULATEBANDSTRUCTURE calculates the bands according to the paths in
%k-space

    h = waitbar(0,'Calculate bandstructure');
    
    
    totalKpoints = 0;
    for j=1:length(obj.k)
        totalKpoints = totalKpoints + size(obj.k{j},2);
    end
    
    
    
    
    phi = -obj.beta;
    R = [cos(phi), -sin(phi);...
         sin(phi), cos(phi)];
    
    counter = 0;
    total_time = 0;
     
    %loop over the different paths
    for j=1:length(obj.k)
        
        bands = zeros(obj.nFrequency, size(obj.k{j},2));
        obj.tComputeStudy{j} = 0;
        
        %loop over different quasi momenta
        for i=1:size(obj.k{j},2)
            
            tic
            
            k = [obj.k{j}(1,i); obj.k{j}(2,i)];
            k = R*k;
            kx = k(1);
            ky = k(2);
         
            model.physics('solid').feature('pc1').set('kFloquet', {num2str(kx) num2str(ky) '0'});
            model.physics('solid').feature('pc2').set('kFloquet', {num2str(kx) num2str(ky) '0'}); 
            model.physics('solid').feature('pc3').set('kFloquet', {num2str(kx) num2str(ky) '0'}); 
            
            model.study('std').run;
            data = mpheval(model,'freq');
            bands(:,i)=data.d1(:,1);
            
            counter = counter+1;
            total_time = total_time + toc;
            
            obj.tComputeStudy{j} = obj.tComputeStudy{j} + toc;
            
            remainingTime = total_time/counter*(totalKpoints-counter);
            t_todo = ['(finishes in ',char( duration(0,0,remainingTime) ),')'];
            waitbar(counter / totalKpoints,h,['Calculate bandstructure ',t_todo]);
            
        end
        obj.bandStructureMech{j} = real(bands);
        
    end
    
    
    
    close(h);
    
end

