function kPath = calcReciprocalPathway(k)
%CALCRECIPROCALPATHWAY calculates the pathway along a path, i.e. the
%distance that has been gone so far in reciprocal space

    kPath=[0];  
    kx = k(1,:);
    ky = k(2,:);
    for i=2:length(kx)
        delta =  sqrt(sum([(kx(i)-kx(i-1)) ; (ky(i)-ky(i-1))].^2));
        kPath  =  [kPath,kPath(i-1)+delta];
    end

end

