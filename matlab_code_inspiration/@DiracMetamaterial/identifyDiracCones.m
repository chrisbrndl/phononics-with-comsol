function identifyDiracCones( obj )
%IDENTIFYDIRACCONES checks out the diracCones and calculats their
%fracyional bandwidth
%   Details....
    

    DEBUG = false;

    if(DEBUG==true)
        figure(100);
        clf;
        obj.plotBandstructure();
    end
    
    for j=1:2
        
        bands = obj.bandStructureMech{j};
    
        
        
        
        
        %frist step: which bands of the AB and CD are in the bandgaps of GM
        idx = [];
        for i=1:size(bands,1) %loop hrough all bands
            for k=1:size(obj.bandgaps,1) %loop through bandgaps
                
                band = bands(i,:);
                
                %check if this band is somehow within a bandgap 
                if(  any((band>obj.bandgaps(k,1)) & (band<obj.bandgaps(k,2)))  )
                   idx = [idx,i];
                end
            
            
            end
        end
        
        
        
        
        
        
        
        
        
        %second step: we must have consecutive pairs
        idx2 = [];
        for i=1:length(idx)-1
            if( idx(i)+1 == idx(i+1) )
               idx2 = [idx2; [idx(i),idx(i+1)]];                
            end
        end
        idx = idx2;
        
       if(DEBUG==true)
            %check it quickly
            subplot( 1,length(obj.bandStructureMech),j);
            for i=1:size(idx,1) 
                plot(obj.kPath{j}, bands(idx(i,1),:),'Color','red', 'Marker','none','LineStyle','--')
                plot(obj.kPath{j}, bands(idx(i,2),:),'Color','red', 'Marker','none','LineStyle','--')
            end
       end
       
       
       
       
       
       
       
       
       %third step: find the quasimomentum with minium distance between the
       %bands
       coneposition = [];
       for i=1:size(idx,1)
            band1 = bands(idx(i,1),:);
            band2 = bands(idx(i,2),:);
            delta = band2 - band1;
            
            [mindelta, temp] = min(delta);
            [maxdelta, ~] = max(delta);
            
            %if mindelta/maxdelta > 0.2 we drop the 'cone'
            if( mindelta/maxdelta > 0.2 )
                coneposition = [coneposition,0];
            else
                coneposition = [coneposition,temp];
            end
       end
   
       idx2 = [];
       coneposition2 = [];
       for i=1:length(coneposition)
            if( coneposition(i)>0 )
                idx2 = [idx2; idx(i,:)];
                coneposition2 = [coneposition2, coneposition(i)];
            end
       end
       idx = idx2;
       coneposition = coneposition2;
       
       if(DEBUG==true)
            %check it quickly
            subplot( 1,length(obj.bandStructureMech),j);
            for i=1:size(idx,1) 
                plot(obj.kPath{j}, bands(idx(i,1),:),'Color','red', 'Marker','none','LineStyle','-')
                plot(obj.kPath{j}, bands(idx(i,2),:),'Color','red', 'Marker','none','LineStyle','-')
            end
       end
       
       
       
       
       
       
       
       
       
       
       %forth step: bands should monotonically in-/decreasefrom coneposition outwards 
       %disp(idx)
       
      
       diracBandsTemp = [];
       coneEnergyTemp = [];
       for i=1:size(idx,1)
            lowerband = bands(idx(i,1),:);
            upperband = bands(idx(i,2),:);
            
            lowerleft = lowerband([1:coneposition(i)]);
            upperleft = upperband([1:coneposition(i)]);
            leftk = obj.kPath{1}([1:coneposition(i)]);
            
            lowerright = lowerband([coneposition(i):length(lowerband)]);
            upperright = upperband([coneposition(i):length(lowerband)]);
            rightk = obj.kPath{1}([coneposition(i):length(lowerband)]);
            
             if(DEBUG==true)
                plot(leftk,lowerleft,'Color','green')
                plot(leftk,upperleft,'Color','blue')
                plot(rightk,lowerright,'Color','yellow')
                plot(rightk,upperright,'Color','cyan')
             end
      
             
             ok1 = issorted(lowerleft); %this one must increase
             ok2 = issorted(fliplr(upperleft)); %this one must decrease
             ok3 = issorted(upperright); %this one must increase
             ok4 = issorted(fliplr(lowerright));%this one must decrease
            
             ok = ok1 * ok2 * ok3 * ok4;
            
             
             if (ok==1)
                diracBandsTemp = [diracBandsTemp; [idx(i,1),idx(i,2)]];
             end    
                 
       end
       diracBands{j} = diracBandsTemp;
       
    end
    
    
    
    %step 5:now we identified the cone, let's calculate their coneFreq
    
    if(DEBUG==true)
        for j=1:length(diracBands)
            subplot( 1,length(obj.bandStructureMech),j);
           
            for i=1:size(diracBands{j},1) 
                idx_lower = diracBands{j}(i,1);
                idx_upper = diracBands{j}(i,2);
                
                plot(obj.kPath{j}, obj.bandStructureMech{j}(idx_lower,:),'Color','red', 'Marker','.','LineStyle','-','LineWidth',1.5)
                plot(obj.kPath{j}, obj.bandStructureMech{j}(idx_upper,:),'Color','red', 'Marker','.','LineStyle','-','LineWidth',1.5)
            end
        end
    end
    
    
    coneFreq1 = [];
    coneFreq2 = [];
    coneIdx1  = [];
    bandWidth1 = [];
    bandWidth2 = [];
    coneIdx2 = [];
    
    for j=1:length(diracBands)
        for i=1:size(diracBands{j},1) 
             idx_lower = diracBands{j}(i,1);
             idx_upper = diracBands{j}(i,2);
               
             lowerband = obj.bandStructureMech{j}(idx_lower,:);
             upperband = obj.bandStructureMech{j}(idx_upper,:);
             
             %calculate the cone frequencies
             coneFreq = (max(lowerband)+min(upperband))/2;
             
             
             %calculate the width-width of the cone
             idx = max(idx_lower-1,1);
             distanceToLowerBand = abs(coneFreq-max( obj.bandStructureMech{j}(idx,:)));
             
             idx = min(idx_upper+1, size( obj.bandStructureMech{j},1));
             distanceToUpperBand = abs(coneFreq-min( obj.bandStructureMech{j}(idx,:)));
             
             distanceToBandGapBoundary = min(abs(obj.bandgaps-coneFreq));
             
             bandwidth = min([distanceToLowerBand,distanceToUpperBand, distanceToBandGapBoundary]);
            
            %save them
             if (j==1)
                coneFreq1 = [coneFreq1, coneFreq];
                bandWidth1 = [bandWidth1, bandwidth];
                coneIdx1 = [coneIdx1, idx_lower];
             else
                coneFreq2 = [coneFreq2, coneFreq];
                bandWidth2 = [bandWidth2, bandwidth];
                coneIdx2 = [coneIdx2, idx_lower];
             end
             
        end
    end
    
    
    
    
    %step 6: Dirac cones should appear in both AB and CD
    
    coneFreq = [];
    bandWidth = [];
    coneIdx = [];
    
    for i=1:length(coneFreq1)
        for j=1:length(coneFreq2)
            delta = abs(coneFreq1(i)-coneFreq2(j));
            coneFreqTemp = 0.5*(coneFreq1(i)+coneFreq2(j));
            
            
            if( delta/coneFreqTemp < 0.01)
                coneFreq = [coneFreq, coneFreqTemp];
                bandWidth = [bandWidth,  2*min(bandWidth1(i),bandWidth2(j)) ];
                coneIdx = [coneIdx, [coneIdx1(i); coneIdx2(j)] ];
                
                break;
            end
            
        
        end
    end
    
    
    obj.coneFreq = coneFreq;
    obj.bandWidth = bandWidth;
    obj.coneIdx = coneIdx;
    obj.fractionalConeBandWidth = bandWidth./coneFreq;
    
    
    
end

