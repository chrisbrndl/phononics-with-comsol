function designMetamaterial(obj,aOptDesired, rOptDesired,spacingToGeo)
%DESIGNMETAMATERIAL makes the obj a metamaterial
%   This function calculates the geometry and the position of the holes
%   according to the desired optical lattice constant aOptDesired, the
%   desired hole radius rOptDesired and the minimum spacing to the
%   macrogeometry spacingToGeo. Thereby it keeps the C6V symmetry. Note
%   that you need to run any generateRandomCX or mutateGeometry beforehand 
%   to generate an object .


    obj.aOptDesired = aOptDesired;
    obj.rOptDesired = rOptDesired;
    obj.spacingToGeo = spacingToGeo;
    
    
    %direction of the lattice vectors of the optical lattice
    v1 = obj.coordHex(:,2) - obj.coordHex(:,1); 
    v2 = obj.coordHex(:,6) - obj.coordHex(:,1);
    
    %sidelength of the hexagon
    l = sqrt(transpose(v1)*v1);
    
    %rescaled optical lattice constant 
    m = round(l/aOptDesired);
    aOpt = l/m;
    
    %accordingly rescaled hole radius
    rOpt = rOptDesired/aOptDesired * aOpt;
    
    %rescale the lattice vetores
    v1 = v1/sqrt(transpose(v1)*v1)*aOpt;
    v2 = v2/sqrt(transpose(v2)*v2)*aOpt;
    
    %save the values 
    obj.aOpt = aOpt;
    obj.rOpt = rOpt;

    %start from the right-upper cirner and fill the structure
    coordHoles = [];
    for i=0:3*m
        for j=0:3*m
            point = obj.coordHex(:,1) + i*v1 + j*v2;
            coordHoles = [coordHoles,point];
        end
    end

   %delete outer points 
   in = inpolygon(coordHoles(1,:),coordHoles(2,:),1.01*obj.coordHex(1,:),1.01*obj.coordHex(2,:));
   idx_inside = find(in); 
   temp = coordHoles;
   coordHoles = [];
   for i=1:length(idx_inside)
        coordHoles = [coordHoles, temp(:,idx_inside(i))];
   end
   
   %generate enlarged geoPolygon
        %first generate an accurate full Bezier polygon (resolution of 100 between BezPoints)
        coordGeoAccurate = [];
        coordResAccurate = 100;
        for i=1:size(obj.coordBezComplete,2)
            j=i+1;
            if(i==size(obj.coordBezComplete,2)) 
                j = 1;
            end
            cubicBezierCurve = obj.generateCubicBezierCurve(obj.coordBezComplete(:,i),obj.coordBezComplete(:,j),coordResAccurate);
            coordGeoAccurate = [coordGeoAccurate, cubicBezierCurve];
        end
   
        %generate an enlarged version of the accurate geometry
        coordGeoEnlarged = obj.genEnlargedPolygon(coordGeoAccurate,spacingToGeo+rOpt);
               
   %delte the iner points
   in = inpolygon(coordHoles(1,:),coordHoles(2,:),coordGeoEnlarged(1,:),coordGeoEnlarged(2,:));
   idx_outside = find(in-1); 
   temp = coordHoles;
   coordHoles = [];
   for i=1:length(idx_outside)
        coordHoles = [coordHoles, temp(:,idx_outside(i))];
   end 
    
   obj.coordHoles = coordHoles;
   obj.coordGeoAccurate = coordGeoAccurate;
   coordGeoEnlarged = obj.genEnlargedPolygon(coordGeoAccurate,spacingToGeo);
   obj.coordGeoEnlarged = coordGeoEnlarged;
    
   %plot(coordHoles(1,:),coordHoles(2,:),'LineStyle','none','Marker','.');
    %plot(coordGeoEnlarged(1,:),coordGeoEnlarged(2,:),'Color','red');
    
end

