classdef DiracMetamaterial < handle
    %DIRACMETAMATEIAL models the unitcell of a nano-optomechanical material
    %
    %   PROPERTIES (real-only)
    %   A macrostructures (lengths are in nanometers, angles in rad)
    %       aMech                   phononic lattice constant
    %       aMechLimits             minimum and maximum aMech
    %       d                       slab thickness
    %       coordHex                (2,n)-dimensional array storing the unitcell's boundary, i.e. the hexagon
    %       coordBez                (4,n)-dimesional array storing the Bezier points
    %       coordBezComplete        (4,n)-dimesional array storing the allBezier points (redundance due to symmetry)
    %       coordRes                resolution between Bezier points (10 seems to be a good number)
    %       coordGeo                (2,n)-dimensional array storing the polygon defining the hole's geometry
    %       coordGeoAccurate        (2,n)-dimensional array storing thepolygon defining the hole's geometry with a higher resolution (coordRes = 100)
    %       coordGeoEnlarged        (2,n)-dimensional array storing an enlarged polygon restricting the holes' positions of the metamaterial
    %       coordUnitSector         (2,n)-dimensional array storing the polygon defining unitsector
    %       areaHex                 area of the whole hexagon
    %       areaGeo                 area of the geometry-hole
    %       materialFraction        fraction of the material = 1-areaGeo/areHex
    %       spacingToHex            minimal distance to the boundary of the unitcell (0.05*a is a good value)
    %       spacingToCenter         minimal distance to the center of the hexagon (0.05*a is a good value)
    %       directPathWayFraction   fraction of direct way and path path (> 0.05)
    %       maxMaterial             the maximum area (in percentage of the unitcell area) which is covered by material (Si)
    %       symmetry                C6V, C3V or C6
    %   B microstructures (lengths are in nanometers)
    %       aOptDesired             desired photonic lattice constant
    %       rOptDesired             desired photonic hole radius
    %       aOpt                    photonic lattice constant
    %       rOpt                    photonic hole radius
    %       spacingToGeo            minimum distance between a hole the geometry hole
    %       coordHoles              (2,n)-dimensional array storing the centers of the holes
    %   C material        
    %       anisotropic             ture is the anisotropic silicon shoule be used. 
    %       beta                    direction of the [100 vector]. beta = 0-> [100] || x-axis, couterclockwiese fashion
    %   D reciprocal space (length are in 1/m)   
    %       kPolygon                cell array that contains (2,n)-dimensional arrays storing differnt paths in reciprocal space       
    %       kLabel                  cell array that contains (2,n)-dimensional arrays storing the corresponding labels       
    %       kRes                    cell array with 1-dimensional array storing the resolution of the parts of the polygon, it has one entry less then kPolygon(:,1)
    %       k                       cell array with the full (m,2)-dimensional arrays stroning all the k-values that will be looped over?
    %       kPath                   cell array of k of the paths (distance along the path)
    %   E comsol mesh (lengths are in nm)
    %       meshMax                 maximum distance of mesh points
    %       meshMin                 minimum distance of mesh points
    %       nMeshLayer              number of mesh layers
    %       tComputeGeo             comutational time for building the geo    
    %       tComputeMesh            comutational time for meshing    
    %   F comsol study (frequencies are in 1/s not rad/s)
    %       centralFrequency        cell array with central frequency 
    %       nFrequency              cell array with the number of desired frequencies
    %       tComputeStudy           cell array with computational time of the study
    %       bandStructureMech       cell array stored band sturture 
    %   G bandstructure analytic
    %       minFractionalBandwidth      minimum fractional bandwidth of a bandgap such that it is considered a bandgap
    %       bandgaps                    bandgaps of the GM path
    %       coneFreq                    frequenies of detected cones
    %       bandWidth                   bandwidth of detected cones
    %       coneIdx                     indices of the bands of the Diraccones (lower band) of band strutcure AB and CD
    %       fractionalConeBandWidth     fractional bandwidth of the cones 
    %
    %
    %   PUBLICE METHODS
    %       generateRandomC6V (static)
    %       generateRandomC3V (static)
    %       generateRandomC6 (static)
    %       designMetamaterial
    %       evaluateStructure
    %       evaluateStructureMutation
    %       designReciprocalSpaceForEvaluation
    %       plotGeometry
    %       plotReciprocalSpace
    %
    %
    %   PRIVATE METHODS
    %       generateCubicBezierCurve (static)
    %       generateHexagon (static)
    %       dublicateC6V (static)
    %       dublicateC3V (static)
    %       dublicateC6 (static)
    %       InterX (static, external)
    %       genEnlargedPolygon (static)
    %       checkSpacingToCenter
    %       checkSpacingToHex 
    %       checkIntersections
    %       checkDirectPathWayFraction
    %       calcReciprocalPathway (static)
    

    %properties
    properties (SetAccess = private)
        aMech;
        aMechLimits;
        d;
        coordHex;
        coordBez;
        coordBezComplete; 
        coordRes;
        coordGeo;
        coordGeoAccurate;
        coordGeoEnlarged;
        coordUnitSector;
        areaHex;
        areaGeo;
        materialFraction;
        spacingToHex;
        spacingToCenter;
        directPathWayFraction;
        maxMaterial;
        symmetry;
        
        aOptDesired;
        rOptDesired;
        aOpt;
        rOpt;
        spacingToGeo;
        coordHoles;
        
        anisotropic;
        beta;
        
        kPolygon;
        kLabel;
        kRes;
        k;
        kPath;
        
        meshMax;
        meshMin;
        nMeshLayer;
        tComputeGeo;
        tComputeMesh;
        
        centralFrequency;
        nFrequency;
        tComputeStudy;
        bandStructureMech;
        
        minFractionalBandwidth;
        bandgaps;
        coneFreq;
        bandWidth;
        coneIdx;
        fractionalConeBandWidth;
    
    end
    
   
    
    
    %public methods
    methods (Access = public, Static)
        obj = generateRandomC6V(aMin,aMax,d,nBez,coordRes,beta,anisotropic,spacingToHex,spacingToCenter,directPathWayFraction,maxMaterial);
        obj = generateRandomC3V(aMin,aMax,d,nBez,coordRes,beta,anisotropic,spacingToHex,spacingToCenter,directPathWayFraction,maxMaterial);
        obj = generateRandomC6(aMin,aMax,d,nBez,coordRes,beta,anisotropic,spacingToHex,spacingToCenter,directPathWayFraction,maxMaterial);
    end
    methods (Access = public)
        designMetamaterial(obj,aOptDesired, rOptDesired,spacingToGeo);
        model = evaluateStructure(obj,res1,res2,dk, meshMax, meshMin, nMeshLayer,centralFrequency,nFrequency,minFractionalBandwidth)
        model = evaluateStructureMutation(obj)
        evaluateStructureFake(obj,fractionalBandWidth)
        designReciprocalSpaceForEvaluation(obj,res1,res2,dk);
        model = generateComsolModel( obj, meshMax, meshMin, nMeshLayer,centralFrequency,nFrequency );    
        plotGeometry( obj, extended, unitsector, bezPoints, restrictions, material);
        plotReciprocalSpace(obj);
        model = calculateBandStructure( obj, model );
        plotBandstructure( obj );
        findBandgaps( obj,minFractionalBandwidth );
        identifyDiracCones( obj );
        plotGeometryBandstructure( obj,extended, unitsector, bezPoints, restrictions, material )
        objMutation = mutate( obj,p );
    end
    
    
    
    %private methods
    methods (Access = private)
        ok = checkSpacingToCenter( obj );
        ok = checkSpacingToHex( obj );
        ok = checkIntersections( obj );
        ok = checkDirectPathWayFraction(obj);
    end
    methods (Access = private, Static)
        cubicBezierCurve = generateCubicBezierCurve(p1,p2,res);
        hexagon = generateHexagon(a);
        coordBezComplete = dublicateC6V( coordBez );
        coordBezComplete = dublicateC3V( coordBez );
        coordBezComplete = dublicateC6( coordBez );
        P = InterX(L1,varargin);
        polygon_enlarged = genEnlargedPolygon( polygon_root, d);
        kPath = calcReciprocalPathway(k)
    end
    
    
    
    
    
end

