function model = evaluateStructure(obj,res1,res2,dk, meshMax, meshMin, nMeshLayer,centralFrequency,nFrequency, minFractionalBandwidth)
%EVALUATESTRUCTURE evaluates the structure and assigns a fitness value
%   res1:               resolution of the cross-path near the K-point (number of intermediate points)
%   res2:               resolution of the path from G to M (number of intermediate points)
%   dk:                 length of one branch of the cross in units of pi/aMech[m]
%   meshMax:            maximum mesh size
%   meshMin:            minimum mesh size 
%   nMeshLayer:         amount of layers (1 should be enough)
%   centralFrequency:   central frequency of the study
%   nFrequency:         number of frequencies to be calculated
%   minFractionalB:     the minimum fractional bandwidth needed to be
%                       supposed as a bandwidth


    %prepare reciprocal space for evaluation
    if(isempty(obj.kPolygon)==true)
        obj.designReciprocalSpaceForEvaluation(res1,res2,dk)
    end

    %build the comsol model
    model = obj.generateComsolModel(meshMax, meshMin, nMeshLayer,centralFrequency,nFrequency);
    
    %calculate the eigenfrequencies
    model = obj.calculateBandStructure( model );
    
    %find bandgaps
    obj.findBandgaps(minFractionalBandwidth);
    
    %identify the cones
    obj.identifyDiracCones();
    
end

