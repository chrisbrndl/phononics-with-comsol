function cubicBezierCurve = generateCubicBezierCurve(p1,p2,res)
%generateCubicBezierCurve 
%   generates the cubic Bezier curve between to points p1 and p2. p1 and p2
%   are (4,1)-matrices with 
%       p?(1,1) = x-values of the point
%       p?(2,1) = y-values of the point
%       p?(3,1) = weight-value (length of the Bezier catilever)
%       p?(4,1) = alpha-value (x-axis is 0 rad and counter-clockwise fashion)
%   res is the resolution of the curve, i.e. by how many points will it be
%   approximated

    
    %starting point 
    xx1 = p1(1,1);
    yy1 = p1(2,1);
    
    %weight point of the starting point
    xx2 = p1(1,1) + p1(3,1)*cos(p1(4,1));
    yy2 = p1(2,1) + p1(3,1)*sin(p1(4,1));
        
    %weight point pf the ending point
    xx3 = p2(1,1) + p2(3,1)*cos(p2(4,1)+pi);
    yy3 = p2(2,1) + p2(3,1)*sin(p2(4,1)+pi);
    
    %end point
    xx4 = p2(1,1);
    yy4 = p2(2,1);
    
    %parameteriztion
    t = linspace(0,1,res);
    s = 1-t;
    
    %bezier line
    x = s.^3 * xx1 + 3*s.^2.*t*xx2 + 3*s.*t.^2*xx3 + t.^3*xx4 ;
    y = s.^3 * yy1 + 3*s.^2.*t*yy2 + 3*s.*t.^2*yy3 + t.^3*yy4 ;
    cubicBezierCurve = [x;y];
    
    
    %to check
    %{
        figure(2)
        clf;
        hold on;
        axis equal;
        plot([xx1,xx2],[yy1,yy2],'Marker','o');
        plot([xx3,xx4],[yy3,yy4],'Marker','o');
        plot( cubicBezierCurve(1,:), cubicBezierCurve(2,:) ,'Marker','.');
    %}
    
end

