function [ polygon_enlarged ] = genEnlargedPolygon( polygon_root, d)

    x_polygon = polygon_root(1,:);
    y_polygon = polygon_root(2,:);

    Bx = x_polygon;
    By = y_polygon;
    
    %disp( ['-------> old:',    num2str(circshift(Bx,1)) ]);
    %disp( ['-------> new:',    num2str( circshift(Bx,[0,1])  )]);
    
    Ax = circshift(Bx,[0,1]);
    Ay = circshift(By,[0,1]);
    Cx = circshift(Bx,[0,-1]);
    Cy = circshift(By,[0,-1]);

    %Calculate the triangles sides
    a = sqrt( (Bx-Cx).^2 + (By-Cy).^2 );
    b = sqrt( (Ax-Cx).^2 + (Ay-Cy).^2 );
    c = sqrt( (Bx-Ax).^2 + (By-Ay).^2 );
    
    %Calculate the InnerCircle Central Point
    Sx = (a.*Ax + b.*Bx + c.*Cx)./(a+b+c);
    Sy = (a.*Ay + b.*By + c.*Cy)./(a+b+c);

    %Calculate unit vector 
    vx = Bx-Sx; 
    vy = By-Sy;
    norm = sqrt(vx.^2+vy.^2);
    vx = vx./norm;
    vy = vy./norm;
    
    %construct new points
    Px = [];
    Py = [];
    for i=1:length(Bx)
        Px = [Px,Bx(i)+d*vx(i),Bx(i)-d*vx(i)];
        Py = [Py,By(i)+d*vy(i),By(i)-d*vy(i)];
    end
    
    %delete inner points 
    in = inpolygon(Px,Py,x_polygon,y_polygon);
    idx_inside = find(in);
    Px(idx_inside) = [];
    Py(idx_inside) = [];

    %distances from enlraged polygon points to originial polygon
    dist = zeros(size(Px));
    for i=1:length(Px)
        dist(i) = min( sqrt( (Bx-Px(i)).^2+(By-Py(i)).^2 ));
    end
    idx_tooNear = (dist<0.999*d);
    Px(idx_tooNear) = [];
    Py(idx_tooNear) = [];
    
    x_enlarged = Px; 
    y_enlarged = Py;

    
    id1 = find(isnan(x_enlarged));
    id2 = find(isnan(y_enlarged));
    x_enlarged([id1,id2]) = [];
    y_enlarged([id1,id2]) = [];
    
    polygon_enlarged = [x_enlarged;y_enlarged];
    
end

