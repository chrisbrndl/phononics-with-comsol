function model = generateComsolModel( obj, meshMax, meshMin, nMeshLayer,centralFrequency,nFrequency )
%GENERATECOMSOLMODEL generates the Comsol model according to the specs

    obj.meshMax = meshMax;
    obj.meshMin = meshMin;
    obj.nMeshLayer = nMeshLayer; 
    obj.centralFrequency = centralFrequency;
    obj.nFrequency = nFrequency;
    
    %import Comsol stuff
    import com.comsol.model.*
    import com.comsol.model.util.*
    
    %create model and geometry node
    model = ModelUtil.create('Model');
    model.modelNode.create('comp1');
    geometry = model.geom.create('geom1',3);
    
    %set the standard length to nm 
    geometry.lengthUnit('nm');
    
    
    tic
    %workplane
    workplane = geometry.feature.create('wp1','WorkPlane');
    workplane.set('quickz','0');
    
    %hexagon
    hexagon = workplane.geom.create('pol1', 'Polygon');
    hexagon.set('source', 'table');
    for i=1:size(obj.coordHex,2)
        hexagon.setIndex('table', num2str( obj.coordHex(1,i) ), i-1, 0);  %value,row, coloumn
        hexagon.setIndex('table', num2str( obj.coordHex(2,i) ), i-1, 1);
    end
     
    %geometry
    polygon = workplane.geom.create('pol2', 'Polygon');
    polygon.set('source', 'table');
    for i=1:size(obj.coordGeo,2)
        polygon.setIndex('table', num2str( obj.coordGeo(1,i) ), i-1, 0);  %value,row, coloumn
        polygon.setIndex('table', num2str( obj.coordGeo(2,i) ), i-1, 1);
    end
    
    %difference
    dif = workplane.geom.create('dif1', 'Difference');
    dif.selection('input').set({'pol1'});
    dif.selection('input2').set({'pol2'});
    
    %extrusion
    ext = geometry.feature.create('ext1', 'Extrude');
    ext.set('workplane', 'wp1');
    ext.selection('input').set({'wp1'});
    ext.setIndex('distance', num2str(obj.d/2), 0);

    
    %build geometry node
    geometry.run
    obj.tComputeGeo = toc;
    
    

    %assign Material
    if(obj.anisotropic == false) 
        Silicon = model.material.create('Silicon');
        Silicon.name('Silicon');
        Silicon.propertyGroup('def').set('relpermeability', '1');
        Silicon.propertyGroup('def').set('electricconductivity', '1e-12[S/m]');
        Silicon.propertyGroup('def').set('relpermittivity', '11.7');
        Silicon.propertyGroup('def').set('density', '2329[kg/m^3]');
        Silicon.propertyGroup.create('Enu', 'Young''s modulus and Poisson''s ratio');
        Silicon.propertyGroup('Enu').set('poissonsratio', '0.28');
        Silicon.propertyGroup('Enu').set('youngsmodulus', '170e9[Pa]');
    else
        SiliconAnisotropic = model.material.create('mat1', 'Common', 'comp1');
        SiliconAnisotropic.label('Si - Silicon (single-crystal, anisotropic)');
        SiliconAnisotropic.set('family', 'custom');
        SiliconAnisotropic.set('lighting', 'cooktorrance');
        SiliconAnisotropic.set('specular', 'custom');
        SiliconAnisotropic.set('customspecular', [0.7843137254901961 1 1]);
        SiliconAnisotropic.set('fresnel', 0.9);
        SiliconAnisotropic.set('roughness', 0.1);
        SiliconAnisotropic.set('shininess', 200);
        SiliconAnisotropic.set('diffuse', 'custom');
        SiliconAnisotropic.set('customdiffuse', [0.6666666666666666 0.6666666666666666 0.7058823529411765]);
        SiliconAnisotropic.set('ambient', 'custom');
        SiliconAnisotropic.set('customambient', [0.6666666666666666 0.6666666666666666 0.7058823529411765]);
        SiliconAnisotropic.set('fresnel', 0.7);
        SiliconAnisotropic.set('roughness', 0.5);
        SiliconAnisotropic.propertyGroup('def').set('density', '2330[kg/m^3]');
        SiliconAnisotropic.propertyGroup.create('Anisotropic', 'Anisotropic');
        SiliconAnisotropic.propertyGroup('Anisotropic').set('D', {'166[GPa]' '64[GPa]' '166[GPa]' '64[GPa]' '64[GPa]' '166[GPa]' '0[GPa]' '0[GPa]' '0[GPa]' '80[GPa]'  ...
                '0[GPa]' '0[GPa]' '0[GPa]' '0[GPa]' '80[GPa]' '0[GPa]' '0[GPa]' '0[GPa]' '0[GPa]' '0[GPa]'  ...
                '80[GPa]'});
        SiliconAnisotropic.set('family', 'custom');
        SiliconAnisotropic.set('lighting', 'cooktorrance');
        SiliconAnisotropic.set('specular', 'custom');
        SiliconAnisotropic.set('customspecular', [0.7843137254901961 1 1]);
        SiliconAnisotropic.set('fresnel', 0.9);
        SiliconAnisotropic.set('roughness', 0.1);
        SiliconAnisotropic.set('shininess', 200);
        SiliconAnisotropic.set('diffuse', 'custom');
        SiliconAnisotropic.set('customdiffuse', [0.6666666666666666 0.6666666666666666 0.7058823529411765]);
        SiliconAnisotropic.set('ambient', 'custom');
        SiliconAnisotropic.set('customambient', [0.6666666666666666 0.6666666666666666 0.7058823529411765]);
        SiliconAnisotropic.set('fresnel', 0.7);
        SiliconAnisotropic.set('roughness', 0.5);
    end
    
    
    
    
    %boundaries
    % NW / \ NE
    % W |   | E
    % SW \ / SE
    
    
    e = 0.01*min([obj.d,obj.aMech]);
    hex = obj.coordHex;
    hex = [hex,hex(:,1)];
   
    for i=1:6
        coord = [hex(1,i),hex(2,i),obj.d/2];
        temp1 = mphselectcoords(model,'geom1',coord','boundary','radius',e);
        coord = [hex(1,i+1),hex(2,i+1),0];
        temp2 = mphselectcoords(model,'geom1',coord','boundary','radius',e);
        if(i==1) NE=intersect(temp1,temp2); end 
        if(i==2) NW=intersect(temp1,temp2); end 
        if(i==3) W=intersect(temp1,temp2); end 
        if(i==4) SW=intersect(temp1,temp2); end 
        if(i==5) SE=intersect(temp1,temp2); end 
        if(i==6) E=intersect(temp1,temp2); end 
    end
    
    coord = [hex(1,1),hex(2,1),0];
    temp1 = mphselectcoords(model,'geom1',coord','boundary','radius',e);
    coord = [hex(1,4),hex(2,4),0];
    temp2 = mphselectcoords(model,'geom1',coord','boundary','radius',e);
    B=intersect(temp1,temp2);

    coord = [hex(1,1),hex(2,1),obj.d/2];
    temp1 = mphselectcoords(model,'geom1',coord','boundary','radius',e);
    coord = [hex(1,4),hex(2,4),obj.d/2];
    temp2 = mphselectcoords(model,'geom1',coord','boundary','radius',e);
    T=intersect(temp1,temp2);
    
    
    
    %physics
    physics = model.physics.create('solid', 'SolidMechanics', 'geom1');
    
    physics.feature.create('pc1', 'PeriodicCondition', 2);
    physics.feature('pc1').set('PeriodicType', 'Floquet');
    physics.feature('pc1').selection.set([NE SW]);
    
    physics.feature.create('pc2', 'PeriodicCondition', 2);
    physics.feature('pc2').set('PeriodicType', 'Floquet');
    physics.feature('pc2').selection.set([E W]);
    
    physics.feature.create('pc3', 'PeriodicCondition', 2);
    physics.feature('pc3').set('PeriodicType', 'Floquet');
    physics.feature('pc3').selection.set([SE NW]);
    
    physics.feature.create('sym1', 'SymmetrySolid', 2);
    physics.feature('sym1').selection.set(B);
   
    if(obj.anisotropic == true ) 
        physics.feature('lemm1').set('SolidModel', 'Anisotropic');
    end
    
    
    
    
    %embedd a cavity if neccessary
    if ( ~isempty(obj.coordHoles) )
        tic
        %obj.designCavity(10); 
        names = {};
        
        %create cylinder
        for i=1:size(obj.coordHoles,2)
            name = ['cyl',num2str(i)];
            geometry.create(name, 'Cylinder');
            geometry.feature(name).set('r', num2str(obj.rOpt) );
            geometry.feature(name).set('pos', {num2str(obj.coordHoles(1,i)) num2str(obj.coordHoles(2,i)) '0'});
            geometry.feature(name).set('h', num2str(obj.d/2)); 
            names = [names,name];
        end
        
        %build difference
        geometry.create('dif1', 'Difference');
        geometry.feature('dif1').selection('input').set({'ext1'});
        geometry.feature('dif1').selection('input2').set(names);
        
        obj.tComputeGeo = obj.tComputeGeo + toc;
    end
    
    
    
    %rotate the whole object (by -beta as we do not rotate the tensor but the geometry)
    %rotating the tensor is somehow buggy
    geometry.create('rot1', 'Rotate');
    if(~isempty(obj.coordHoles))
        geometry.feature('rot1').selection('input').set({'dif1'});
    else
        geometry.feature('rot1').selection('input').set({'ext1'});
    end
    geometry.feature('rot1').set('rot', num2str(-obj.beta/pi*180));
    
    
    
    
    %mesh   
    tic;
    mesh = model.mesh.create('mesh1', 'geom1');
    mesh.create('ftri1', 'FreeTri');
    mesh.feature('ftri1').selection.set(B);
    mesh.feature('ftri1').create('size1', 'Size');
    mesh.feature('ftri1').feature('size1').set('custom', 'on');
    mesh.feature('ftri1').feature('size1').set('hmaxactive', 'on');
    mesh.feature('ftri1').feature('size1').set('hmax', num2str(obj.meshMax));
    mesh.feature('ftri1').feature('size1').set('hmin', num2str(obj.meshMin));
    mesh.feature('ftri1').feature('size1').set('hminactive', 'off');
    mesh.feature('ftri1').set('method', 'af');
    mesh.run;

    mesh.create('swe1', 'Sweep');
    mesh.feature('swe1').selection('sourceface').set(B);
    mesh.feature('swe1').selection('targetface').set(T);
    model.mesh('mesh1').feature('swe1').create('dis1', 'Distribution');
    mesh.feature('swe1').feature('dis1').set('numelem', num2str(obj.nMeshLayer));
    mesh.run;
    
    obj.tComputeMesh = toc;

  
   
    %study
    study = model.study.create('std');
    studyEf = study.feature.create('eig', 'Eigenfrequency');
    studyEf.activate('solid', true);
    studyEf.set('neigs', num2str(obj.nFrequency));
    studyEf.set('shift', num2str(obj.centralFrequency));
   
    
end

