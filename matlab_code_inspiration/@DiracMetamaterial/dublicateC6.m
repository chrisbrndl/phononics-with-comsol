function coordBezComplete = dublicateC6( coordBez )
%DUBLICATEC6V duplicates n BezierPoints by assuming a C6 symmerty
%   It results an array of all the Bezier points after rotating 5 times 
%   by 60 degree.


        x = coordBez(1,:);
        y = coordBez(2,:);
        weight = coordBez(3,:);
        alpha = coordBez(4,:);

        n = size(coordBez,2);
        
        
        %rotate the whole thing 5 times (counter clockwise)
        phi = pi/3;
        R = [cos(phi), -sin(phi);...
             sin(phi), cos(phi)];
        xr = x;
        yr = y;
        weightr = weight;
        alphar = alpha;
        for i=1:5
            for j=1:n
                temp = R^i*[x(j);y(j)];
                xr = [xr, temp(1)];
                yr = [yr, temp(2)];
                alphar = [alphar, alpha(j)+i*phi];
                weightr = [weightr, weight(j)];
            end
        end
        
        
        coordBezComplete = [xr; yr; weightr; alphar];

end

