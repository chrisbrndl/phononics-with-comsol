function model = evaluateStructureMutation(obj)
%EVALUATESTRUCTUREMUTATION evaluates the structure of muatation (settings
%like meshMax and so an are already copied from the parent)

    %build the comsol model
    model = obj.generateComsolModel(obj.meshMax, obj.meshMin, obj.nMeshLayer,obj.centralFrequency,obj.nFrequency);
    
    %calculate the eigenfrequencies
    model = obj.calculateBandStructure( model );
    
    %find bandgaps
    obj.findBandgaps(obj.minFractionalBandwidth);
    
    %identify the cones
    obj.identifyDiracCones();
    
end

