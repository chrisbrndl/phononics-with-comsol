function coordBezComplete = dublicateC3V( coordBez )
%DUBLICATEC3V duplicates n BezierPoints by assuming a C3V symmerty
%   It results an array of all the Bezier points after mirroring on the
%   lower inner boundary of the unitsector and afterwards rotating
%   everything by 120 degrees twice
        

        

        x = coordBez(1,:);
        y = coordBez(2,:);
        weight = coordBez(3,:);
        alpha = coordBez(4,:);

        n = size(coordBez,2);
        
        
        
        phi = pi/6;
        R30 = [cos(phi), -sin(phi);...
               sin(phi), cos(phi)];
        
        
        %mirror them on the -30degree axis
        
        %first rotate te points by 30 degree
        xm = [];
        ym = [];
        alpham = [];
        weightm = [];
        for i=1:n
            temp = R30*[x(i);y(i)];
            xm = [xm, temp(1)];
            ym = [ym, temp(2)];
            alpham = [alpham, alpha(i)+pi/6];
            weightm = [weightm,weight(i)];
        end
        
        %mirror the on the x-axis
        xm = [fliplr(xm),xm];
        ym = [-fliplr(ym),ym];
        weightm = [fliplr(weightm),weightm];
        alpham = [-fliplr(alpham)+pi,alpham];
     
        %rotate the points by -30 degree
        xmm = [];
        ymm = [];
        alphamm = [];
        weightmm = [];
        for i=1:2*n
            temp = transpose(R30)*[xm(i);ym(i)];
            xmm = [xmm, temp(1)];
            ymm = [ymm, temp(2)];
            alphamm = [alphamm, alpham(i)-pi/6];
            weightmm = [weightmm,weightm(i)];
        end
        xm = xmm;
        ym = ymm;
        weightm = weightmm;
        alpham = alphamm;
        
        
        
        
        
        
        
        
        
        %rotate the whole thing 2 times (counter clockwise)
        phi = 2*pi/3;
        R = [cos(phi), -sin(phi);...
             sin(phi), cos(phi)];
        xmr = xm;
        ymr = ym;
        weightmr = weightm;
        alphamr = alpham;
        for i=1:2
            for j=1:2*n
                temp = R^i*[xm(j);ym(j)];
                xmr = [xmr, temp(1)];
                ymr = [ymr, temp(2)];
                alphamr = [alphamr, alpham(j)+i*phi];
                weightmr = [weightmr, weightm(j)];
            end
        end
        
        
        coordBezComplete = [xmr; ymr; weightmr; alphamr];
      
end

