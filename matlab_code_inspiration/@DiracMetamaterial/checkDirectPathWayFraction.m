function ok = checkDirectPathWayFraction(obj)
%checkWayFraction
%   ...
 
    x_hole = obj.coordGeo(1,:);
    y_hole = obj.coordGeo(2,:);
    f_crit = obj.directPathWayFraction;

    %Direct distance between two points
    X = repmat( transpose(x_hole), 1, length(x_hole)  );
    Y = repmat( transpose(y_hole), 1, length(y_hole)  );
    D = sqrt((X-transpose(X)).^2 + (Y-transpose(Y)).^2);
  
    
    %Circumference of the polygon
    %{
    C = 0;
    for i=1:length(x_hole)/4
        C = C+ D(i,i+1);
    end
    C=C*4;
    %}
    DIAG = diag(ones(1,length(x_hole)-1),1) + diag([1],length(x_hole)-1);
    C = D.*DIAG;
    C = sum(C(:));
    
    %Pathway distance between two points
    P = zeros(size(D));
    for i=1:length(x_hole)
        for j=i+1:length(x_hole)  
            P_temp = 0;
            for x=i:j-1
                P_temp =  P_temp + D(x,x+1);
            end
            P_temp = min(P_temp, C-P_temp);
            P(j,i) = P_temp;
            P(i,j) = P_temp;
        end
    end
    F = D./P; %Fraction between direct way and pathway 
    
    %[val,idx1D]=max(D);
    %[val,idx2D]=max(val);
    %idx1D = idx1D(idx2D);
    
    %[val,idx1P]=max(P);
    %[val,idx2P]=max(val);
    %idx1P = idx1P(idx2P);
    
    [f,idx1]=min(F);
    [f,idx2]=min(f);
    idx1 = idx1(idx2);
   
     
    ok = 0;
    if(f>f_crit)
        ok = 1;
    end
    

    

end










