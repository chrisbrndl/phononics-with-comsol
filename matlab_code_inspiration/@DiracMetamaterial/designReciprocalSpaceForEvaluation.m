function designReciprocalSpaceForEvaluation(obj,res1,res2,dk)
%DESIGNRECIPROCALSPACEFOREVALUATION prepares the k-values to evaluate the
%structure

    %length conversion
    a = obj.aMech*1e-9; 
    dkk = pi/a * dk;

    %high symmetry points
    G = [0;0];
    K = pi/a * [4/3;0];
    M = pi/a * [1; -1/sqrt(3)];
 
    %first path (from left of K to right of K = from A to B)
    A = K-[1;0]*dkk;
    B = K+[1;0]*dkk;
    kPolygon{1} = [A,B];
    kLabel{1} = {'A', 'B'};
    kRes{1} = res1;
    k{1} = [linspace(A(1),B(1),2+res1); linspace(A(2),B(2),2+res1)] ;
    kPath{1} = obj.calcReciprocalPathway(k{1});
       
    %second path (from below K to above K = from C to D)
    C = K-[0;1]*dkk;
    D = K+[0;1]*dkk;
    kPolygon{2} = [C,D];
    kLabel{2} = {'C', 'D'};
    kRes{2} = res1;
    k{2} = [linspace(C(1),D(1),2+res1); linspace(C(2),D(2),2+res1)] ;
    kPath{2} = obj.calcReciprocalPathway(k{2});
    
    %second path (from Gamma to M)
    kPolygon{3} = [G,M];
    kLabel{3} = {'G', 'M'};
    kRes{3} = res2;
    k{3} = [linspace(G(1),M(1),2+res2); linspace(G(2),M(2),2+res2)] ;
    kPath{3} = obj.calcReciprocalPathway(k{3});
    
    %save it
    obj.kPolygon = kPolygon;
    obj.kLabel = kLabel;
    obj.kRes = kRes;
    obj.k = k;
    obj.kPath = kPath;
    
    
    %{
    figure(3)
    clf;
    axis equal; 
    hold on;
    plot(k{1}(1,:),k{1}(2,:),'Marker','o');
    plot(k{2}(1,:),k{2}(2,:),'Marker','o');
    plot(k{3}(1,:),k{3}(2,:),'Marker','o');
    %}
    
     
    
end

