function coordBezComplete = dublicateC6V( coordBez )
%DUBLICATEC6V duplicates n BezierPoints by assuming a C6V symmerty
%   It results an array of all the Bezier points after mirroring on the
%   x-axis and rotating 5 times by 60 degree.


        x = coordBez(1,:);
        y = coordBez(2,:);
        weight = coordBez(3,:);
        alpha = coordBez(4,:);

        n = size(coordBez,2);
        
        %mirror them on the x-axis 
        xm = [fliplr(x),x];
        ym = [-fliplr(y),y];
        weightm = [fliplr(weight),weight];
        alpham = [-fliplr(alpha)+pi,alpha];
        
        
        %rotate the whole thing 5 times (counter clockwise)
        phi = pi/3;
        R = [cos(phi), -sin(phi);...
             sin(phi), cos(phi)];
        xmr = xm;
        ymr = ym;
        weightmr = weightm;
        alphamr = alpham;
        for i=1:5
            for j=1:2*n
                temp = R^i*[xm(j);ym(j)];
                xmr = [xmr, temp(1)];
                ymr = [ymr, temp(2)];
                alphamr = [alphamr, alpham(j)+i*phi];
                weightmr = [weightmr, weightm(j)];
            end
        end
        
        
        coordBezComplete = [xmr; ymr; weightmr; alphamr];

end

