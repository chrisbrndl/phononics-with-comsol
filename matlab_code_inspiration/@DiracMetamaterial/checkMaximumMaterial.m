function ok = checkMaximumMaterial( obj )
%CHECKMAXIMUMMATERIAL checks, that the material is not to much (mostly for
%comutaional reasons)

    

    AHex = polyarea(obj.coordHex(1,:),obj.coordHex(2,:));
    AGeo = polyarea(obj.coordGeo(1,:),obj.coordGeo(2,:));

    materialFraction = 1-AGeo/AHex;
    obj.materialFraction = materialFraction;
    
    
    if (materialFraction <= obj.maxMaterial)
        ok = 1; 
    else
        ok = 0;
    end
    
end

