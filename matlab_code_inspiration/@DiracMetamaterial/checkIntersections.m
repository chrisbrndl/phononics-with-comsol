function ok = checkIntersections( obj )
%checkIntersections 
%   Checks if the Beziercurve intersects with itself
    L1 = [obj.coordGeo(1,:);obj.coordGeo(2,:)];
    C = obj.InterX(L1);
    ok = 0;
    if( size(C,2)==0 )
        ok = 1;
    end
end

