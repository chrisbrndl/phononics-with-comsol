function ok = checkSpacingToHex( obj )
%checkSpacingToCenter
%   checks the distance to the center

    smallerHex = obj.generateHexagon(obj.aMech-obj.spacingToHex);
    
    %if the point is inside in will be 1 at the position 
    in = inpolygon(obj.coordGeo(1,:),obj.coordGeo(2,:),smallerHex(1,:),smallerHex(2,:));
    
    %if there is any 0 in the in-array the sturcture is not okay
    if(min(in)==0)
        ok=0;
    else
        ok=1;
    end
    
end

