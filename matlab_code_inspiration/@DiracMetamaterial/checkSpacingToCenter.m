function ok = checkSpacingToCenter( obj )
%checkSpacingToCenter
%   checks the distance to the center

    %define the restricting circle
    phi = linspace(0,2*pi);
    x_circle = obj.spacingToCenter * sin(phi);
    y_circle = obj.spacingToCenter * cos(phi);  
    
    %if the point is inside in will be 1 at the position 
    in = inpolygon(obj.coordGeo(1,:),obj.coordGeo(2,:),x_circle,y_circle);
    
    %if there is any 1 in the in-array the sturcture is not okay
    if(max(in)==0)
        ok=1;
    else
        ok=0;
    end
    
end

