function plotReciprocalSpace(obj)
%PLOTRECIPROCALSPACE plots the kSpace


    hold on; 
    axis equal;

    color = [0    0.4470    0.7410;...
        0.8500    0.3250    0.0980;...
        0.9290    0.6940    0.1250;...
        0.4940    0.1840    0.5560;...
        0.4660    0.6740    0.1880;...
        0.3010    0.7450    0.9330;...
        0.6350    0.0780    0.1840];
    
    %plot the Brillouinzone
    a = obj.aMech*1e-9;
    G  = [0;0];
    K  = pi/a*[4/3;0]; 
    M  = pi/a*[1;-1/sqrt(3)];

    Rot60 = [cos(pi/3), -sin(pi/3);...
             sin(pi/3), cos(pi/3)];
       
    K2 = Rot60*K;
    K3 = Rot60*K2;
    K4 = Rot60*K3;
    K5 = Rot60*K4;
    K6 = Rot60*K5;  
    BZ = [K,K2,K3,K4,K5,K6];

    plot([BZ(1,:),BZ(1,1)],[BZ(2,:),BZ(2,1)], 'Color','black');
    
      
    %plot the various paths
    for i=1:length(obj.k)
        plot(obj.k{i}(1,:),obj.k{i}(2,:),'Marker','o','Color',color(i,:));
        
        for j=1:length(obj.kLabel{i})
            t = text( obj.kPolygon{i}(1,j),obj.kPolygon{i}(2,j),obj.kLabel{i}(j) );
            t.FontSize = 16;
            t.Color = color(i,:);
            t.FontWeight = 'bold';
        end
    end
    
    axis off;
    %set(gcf, 'InvertHardcopy', 'off')
    
end

