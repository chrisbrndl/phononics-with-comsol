function findBandgaps( obj, minFractionalBandwidth )
%FINDBANDGAPS finds out the bandgaps of the 3rd band strutcure (path G-M)


    %frequencyLimit
	maxFrequencies = [];
    for j=1:length(obj.bandStructureMech)
        temp = max(obj.bandStructureMech{j}(:));
        maxFrequencies = [maxFrequencies, temp];
    end
    frequencyLimit = min(maxFrequencies);

    
    %we check the 3rd BS (path G-M)
    bands = obj.bandStructureMech{3};
    
    
    %loop through all bands 
    bandgaps = [];
    for i=0:size(bands,1)-1
       
        
        %maximum frequency of the i-th band
        if(i==0)
           maxFreq = 0; 
        else
           maxFreq = max(bands(i,:));
        end
        
        %minimum frequency of the i+1-th band
        minFreq = min(bands(i+1,:));
        
        %if the minFreq is larger the maxFreq we have a bandgap
        %the fractional bandwidth should be at least minFractionalBandwidth
        if( minFreq > maxFreq  &&  minFreq-maxFreq > minFractionalBandwidth*(minFreq+maxFreq)/2)
            %disp( [num2str(minFreq), '    ', num2str(maxFreq)])
            bandgaps = [bandgaps; [maxFreq minFreq]];
        end
        
    end
    
    obj.minFractionalBandwidth = minFractionalBandwidth;
    obj.bandgaps = bandgaps;
    
    
    

    
    
    %{
    figure(100);
    clf 
    obj.plotBandstructure();
    
    for i=1:size(bandgaps,1)
        
       x = 0;
       y = bandgaps(i,1);
       dx = max(obj.kPath{3});
       dy = bandgaps(i,2)-bandgaps(i,1);
        
       r = rectangle('Position',[x y dx dy]);
       r.LineStyle = 'none';
       r.FaceColor =  [0.8 0.8 0.8 0.4];
      
       
    end
    %}
    
  
    
    
end

