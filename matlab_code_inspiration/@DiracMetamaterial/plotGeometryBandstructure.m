function plotGeometryBandstructure( obj,extended, unitsector, bezPoints, restrictions, material )
%PLOTBANDSTRUCTURE plots the bandstructures all all defines paths

    blue = [0    0.4470    0.7410];
    red = [ 0.6350    0.0780    0.1840];
    
    
    
    n = 3;
    subplot(1,length(obj.bandStructureMech)+3,[1:n]);
    obj.plotGeometry( extended, unitsector, bezPoints, restrictions, material)
    box on; 
    
    fcb = round(max(obj.fractionalConeBandWidth)*10000)/100;
    
    title(['fractional cone bandwidth: ',num2str( fcb ),' %']);
     
	%frequencyLimit
	maxFrequencies = [];
    for j=1:length(obj.bandStructureMech)
        temp = max(obj.bandStructureMech{j}(:));
        maxFrequencies = [maxFrequencies, temp];
    end
    frequencyLimit = min(maxFrequencies);
    
    
    %indicidual paths 
    for j=1:length(obj.bandStructureMech)
        
        subplot( 1,length(obj.bandStructureMech)+n,j+n);
        hold on;
        
        %bands
        bands = obj.bandStructureMech{j};
        for i=1:size(bands,1) 
           plot(obj.kPath{j}, bands(i,:),'Color',blue, 'Marker','.')
        end
        
        
        %ticks and labels
        ax = gca;        
        ticks = [obj.kPath{j}(1)];
        currentPosition = 1;
        for i=1:length(obj.kRes{j})
            currentPosition = currentPosition + obj.kRes{j}(i) + 1;
            ticks = [ticks, obj.kPath{j}(currentPosition)];
        end
        ax.XTick = ticks;
        ax.XTickLabel = obj.kLabel{j};
        ax.XLim = [ticks(1),ticks(end)];
        ax.YLim = [0, frequencyLimit];
        box on;
        
        %title
        time = char( duration(0,0,obj.tComputeStudy{j}) );
        title(['t=',time])
        
        %plot bandgaps
        %if(j==3)
            for i=1:size(obj.bandgaps,1)
                x = 0;
                y = obj.bandgaps(i,1);
                dx = max(obj.kPath{3});
                dy = obj.bandgaps(i,2)-obj.bandgaps(i,1);
        
                r = rectangle('Position',[x y dx dy]);
                r.LineStyle = 'none';
                r.FaceColor =  [blue, 0.1];
                
            end
        %end
        
        %plot Dirac cones 
        for i=1:size(obj.coneIdx,2)
            if(j==1 || j==2)
               
                x = 0;
                y = obj.coneFreq(i)-0.5*obj.bandWidth(i);
                dx = max(obj.kPath{j});
                dy = obj.bandWidth(i);
        
                r = rectangle('Position',[x y dx dy]);
                r.LineStyle = 'none';
                r.FaceColor = [ 0.6350    0.0780    0.1840 0.3];
                
                plot(obj.kPath{j}, bands(obj.coneIdx(j,i),:),'Color',red, 'Marker','.','LineWidth',1.5 )
                plot(obj.kPath{j}, bands(obj.coneIdx(j,i)+1,:),'Color',red, 'Marker','.','LineWidth',1.5 )
               
            end
        end
        
        
        
        
        
    end
    

end

