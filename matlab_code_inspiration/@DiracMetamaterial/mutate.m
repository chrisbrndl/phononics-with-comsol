function objMutation = mutate( obj,p )
%MUTATE mutates the geometry by p percent
%   here we change the lattice constant by a value within the interval of
%   +-p percent of the inital range 


    DEBUG = false;

    objMutation = DiracMetamaterial();

    %copy some parameters
    objMutation.aMechLimits             = obj.aMechLimits;
    objMutation.d                       = obj.d;
    objMutation.coordRes                = obj.coordRes;
    objMutation.materialFraction        = obj.materialFraction;
    objMutation.spacingToHex            = obj.spacingToHex;
    objMutation.spacingToCenter         = obj.spacingToCenter;
    
    objMutation.directPathWayFraction   = obj.directPathWayFraction;
    objMutation.maxMaterial             = obj.maxMaterial;
    objMutation.symmetry                = obj.symmetry;
    objMutation.aOptDesired             = obj.aOptDesired;
    objMutation.rOptDesired             = obj.rOptDesired;
    
    objMutation.spacingToGeo            = obj.spacingToGeo;
    objMutation.anisotropic             = obj.anisotropic;
    objMutation.beta                    = obj.beta;
    objMutation.nMeshLayer              = obj.nMeshLayer;
    objMutation.nFrequency              = obj.nFrequency;
    objMutation.centralFrequency        = obj.centralFrequency;
    objMutation.minFractionalBandwidth  = obj.minFractionalBandwidth;
    
    objMutation.kLabel                  = obj.kLabel;
    objMutation.kRes                    = obj.kRes;
    
    
    %change aMech by p percent
    ok = false;
    while(ok == false)
        r = 2*(rand()-0.5);
        da = r*p*(max(obj.aMechLimits)-min(obj.aMechLimits));
        
        aMech = obj.aMech + da;
        
        if( aMech>min(obj.aMechLimits) && aMech<max(obj.aMechLimits))
            ok = true; 
        end
    end
    objMutation.aMech = aMech;
    
    
    %build the hexagon (boundary)
    objMutation.coordHex = objMutation.generateHexagon( objMutation.aMech);

    
    
    %unit sector
    if( strcmp(objMutation.symmetry,'C6V') )
        unitsector =   [0, aMech/2, aMech/2;...
                0,       0, aMech/2/sqrt(3)];
        objMutation.coordUnitSector = unitsector;
    else
         unitsector =   [0, aMech/2, aMech/2;...
                        0, -aMech/2/sqrt(3), aMech/2/sqrt(3)];
        objMutation.coordUnitSector = unitsector;
    end
    
    
    
    
    ok = 0;
    if(DEBUG)   counter = 0;   end

    while (ok==0)
        if(DEBUG)
            counter = counter +1; 
            disp(['new mutation',num2str(counter)]);
        end
        
        
        
        
        
        % mutate the Bezier-Points
        if(DEBUG)   tic;   end
        
        scalingFactor = objMutation.aMech / obj.aMech;
        
        inside = 0;
        while(inside == 0)
            r = 2*(rand()-0.5);
            x = scalingFactor*obj.coordBez(1,:) + aMech/2*r*p; 

            r = 2*(rand()-0.5);
            y = scalingFactor*obj.coordBez(2,:) + aMech/2/sqrt(3)*r*p; 

            in = inpolygon(x,y,unitsector(1,:),unitsector(2,:));
            inside = min(in);
        end

        r = 2*(rand()-0.5);
        weight =  scalingFactor*obj.coordBez(3,:) +  aMech/4*r*p;

        r = 2*(rand()-0.5);
        alpha = obj.coordBez(4,:) + 2*pi *r*p;
        
 
        
        
        %generate the coordBez attribute 
        objMutation.coordBez = [x; y; weight; alpha];
        if( strcmp(objMutation.symmetry,'C6V') )
            objMutation.coordBezComplete = objMutation.dublicateC6V( objMutation.coordBez );
        elseif( strcmp(objMutation.symmetry,'C6') )
            objMutation.coordBezComplete = objMutation.dublicateC6( objMutation.coordBez );
        elseif( strcmp(objMutation.symmetry,'C3V') )
            objMutation.coordBezComplete = objMutation.dublicateC3V( objMutation.coordBez );
        end
        
        %generate the full Bezier polygon
        objMutation.coordGeo = [];
        for i=1:size(objMutation.coordBezComplete,2)

            j=i+1;
            if(i==size(objMutation.coordBezComplete,2)) 
                j = 1;
            end

            cubicBezierCurve = objMutation.generateCubicBezierCurve(objMutation.coordBezComplete(:,i),objMutation.coordBezComplete(:,j),objMutation.coordRes);
            objMutation.coordGeo = [objMutation.coordGeo, cubicBezierCurve];
        end

        objMutation.areaHex = polyarea(objMutation.coordHex(1,:),objMutation.coordHex(2,:));
        objMutation.areaGeo = polyarea(objMutation.coordGeo(1,:),objMutation.coordGeo(2,:));

        if(DEBUG)   
            time = toc;
            disp(['     phase1:generation      ',num2str(round(time)),' s']);
        end

        %check spacingToCenter
        if(DEBUG)   tic;   end
        ok1 = objMutation.checkSpacingToCenter();
        if(DEBUG)   disp(['     phase2:check1       ',num2str(ok1),'  ',num2str(round(toc)),' s']); end

        %check spacingToHex 
        if(DEBUG)   tic;   end
        ok2 = objMutation.checkSpacingToHex();
        if(DEBUG)   disp(['     phase2:check2       ',num2str(ok2),'  ',num2str(round(toc)),' s']); end

        %check intersections
        if(DEBUG)   tic;   end
        if(ok1*ok2 == 1)
            ok3 = objMutation.checkIntersections();
        else
            ok3 = 0;
        end
        if(DEBUG)   disp(['     phase2:check3       ',num2str(ok3),'  ',num2str(round(toc)),' s']); end


        %check pathway fractions
        if(DEBUG)   tic;   end
        if(ok1*ok2*ok3 == 1)
            ok4 = objMutation.checkDirectPathWayFraction();
        else
            ok4 = 0;
        end
        if(DEBUG)   disp(['     phase2:check4       ',num2str(ok4),'  ',num2str(round(toc)),' s']); end

        %check maximum material area
        if(DEBUG)   tic;   end
        if(ok1*ok2*ok3*ok4 == 1)
            ok5 = objMutation.checkMaximumMaterial();
        else
             ok5 =0;
        end
        if(DEBUG)   disp(['     phase2:check5       ',num2str(ok5),'  ',num2str(round(toc)),' s']); end

        ok = ok1*ok2*ok3*ok4*ok5;

    end    
          
   

  
  
    
    
    
    
    
    %if it is a metamaterial
    if(isempty(objMutation.aOptDesired) == false )
        %metamaterial
        objMutation.designMetamaterial(objMutation.aOptDesired, objMutation.rOptDesired,objMutation.spacingToGeo);

    end
    
    %rescale reciprocal space  
    factor = obj.aMech / objMutation.aMech;
    for i=1:length(obj.kPolygon)
        objMutation.kPolygon{i} = factor * obj.kPolygon{i};
        objMutation.k{i}        = factor * obj.k{i};
        objMutation.kPath{i}    = factor * obj.kPath{i};
    end
    
    %meshing (scales like the optical lattice constant)
    objMutation.meshMax = objMutation.rOpt/obj.rOpt * obj.meshMax;
    objMutation.meshMin = objMutation.rOpt/obj.rOpt * obj.meshMin;
    
  
end

