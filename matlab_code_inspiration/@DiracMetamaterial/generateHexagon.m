function hexagon = generateHexagon(a)
%GENERATEHEXGON generates a hexagon
%   a is the lattice constant, it is assumed that alpha is 0

    hexagon = [ a/2,         0,         -a/2        ,-a/2,         0,          a/2;...          %x-values
            a/2/sqrt(3), a/sqrt(3), a/2/sqrt(3) ,-a/2/sqrt(3), -a/sqrt(3), -a/2/sqrt(3)];   %y-values

end

