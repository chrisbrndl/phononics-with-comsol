classdef Evolution < handle
    %EVOLUTION models thes genetic evolution of the DiracMetamaterial
    %
    %   ATTRUBUTES
    %   file                target file where the evo-data is saved
    %   batchSize           number of geometries in one batch
    %   maxBatches          maximum number of batches within a generation
    %   mutationStrength    deviation of the structure
    %   seedGeometry        path of the seed structures 
    %   generation          cell array storing the geometries an an array 
    %   status              'wating', 'finished' or 'processing'
    %
    %   METHODS
    %   runEvolution()
    %   plotEvolution()
    
    
    
    properties
        file;
        batchSize; 
        maxBatches;
        mutationStrength;
        seedGeometry;
        generation;
        status;
    end
    
    
    
    
    methods
        
        function obj = Evolution( file, batchSize, maxBatches, mutationStrength, seedGeometry )
            %check if mutation strength is decreasing
            
            if( issorted(fliplr(mutationStrength)) )
                obj.file = file;
                obj.batchSize = batchSize;
                obj.maxBatches = maxBatches;
                obj.mutationStrength = mutationStrength;
                obj.seedGeometry = seedGeometry;
                obj.status = 'waiting';
            else
                disp('mutationStength does not decrease! Please change!');
            end
        end

        run(obj,DEBUG,PRINT);
        
       %run2(obj,DEBUG,PRINT);
        
        out = print(obj,varargin);
        
        setStatus( obj,status );
       
    end
    
end

