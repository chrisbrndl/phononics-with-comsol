function out = print( evo, varargin )
%PRINT prints the evolution
%   color1      color [r,g,b] of the children
%   color2      color [r,g,b] of the parents
%   markerSize  makerSize of the nodes
%   txtSize    if 0 there will be no text
%outputs an array [generation,child] that encodes the path down to the best geometry    

   
    genOut = [];
    chiOut = [];

    if(~isempty(varargin))
        color1 = varargin{1};
    else
        color1 = [0.6 ,0.6 ,0.6];
    end
   
    if(length(varargin)>1)
        color2 = varargin{2};
    else
        color2 = [0.9, 0.3, 0.4];
    end
    
    if(length(varargin)>2)
        markerSize = varargin{3};
    else
         markerSize = 8;
    end
    
    if(length(varargin)>3)
        txtSize = varargin{4};
    else
         txtSize = 10;
    end
    
    minx = +1e10;
    maxx = -1e10;
    
    
    hold on ; 
    
    for r=1:2
    parentxy = [0;0];
    parentFitness = max(evo.generation(1).parent.fractionalConeBandWidth);
    for i=1:length(evo.generation)

        %how is the best 
        jBest = 0;
        temp = 0;
        for j=1:length(evo.generation(i).children)
            if(max(evo.generation(i).children(j).fractionalConeBandWidth)>temp)
                temp = max(evo.generation(i).children(j).fractionalConeBandWidth);
                jBest = j;
            end
            
        end
        
        for j=1:length(evo.generation(i).children)
            
            yoffset = (length(evo.generation(i).children)-1)/2;
            childxy = [j-1-yoffset+parentxy(1), -i];
            
            if(childxy(1) < minx) minx = childxy(1); end
            if(childxy(1) > maxx) maxx = childxy(1); end
            
            fitness = max(evo.generation(i).children(j).fractionalConeBandWidth);
            
            %plot the connections first
            if(r==1)
                con = plot([parentxy(1),childxy(1)],[parentxy(2),childxy(2)]);
                con.Color = color1; 
                if( fitness > parentFitness)
                    con.Color = color2; 
                    con.LineWidth = 2;
                end
            end
            
            %plot the dots in a second run
            if(r==2)
                
                dot = plot(childxy(1),childxy(2), 'Marker','o');
                dot.MarkerFaceColor = color1; 
                dot.MarkerEdgeColor = color1; 
                dot.MarkerSize = markerSize;
                if( j == jBest )
                    dot.MarkerEdgeColor = color2; 
                end
                if( fitness > parentFitness )
                    dot.MarkerFaceColor = color2; 
                end
               
                if(txtSize > 0)
                    txt = text(childxy(1),childxy(2), ['  ',num2str(round(fitness*10000)/100),'%']);
                    txt.FontSize = txtSize;
                end
                
                if(j==length(evo.generation(i).children) && txtSize > 0)    
                    mutStr = evo.generation(i).mutationStrength;
                    txt = text(childxy(1)+0.5,childxy(2)+0.5, ['  \Delta=',num2str(mutStr*100),'%']);
                    txt.FontSize = txtSize;
                    txt.FontWeight = 'bold';
                end
                
            end
            
            %check if this one will be next parent
            if( i< length(evo.generation) )
                if( evo.generation(i+1).parentIdx > 0  )
                    if( evo.generation(i+1).parentIdx == j)
                        nextParentxy = childxy;
                        nextParentFitness = max(evo.generation(i).children(j).fractionalConeBandWidth);
                    end
                end
            end
            
        end
        
        
        if( i< length(evo.generation) )
           if( evo.generation(i+1).parentIdx>0  )
                parentxy = nextParentxy;
                parentFitness=nextParentFitness;
                if(r==1)
                    genOut = [genOut;i];
                    chiOut = [chiOut;jBest];
                end
           end
        end
        
    end
    end
    
    %print the root
    dot = plot(0,0, 'Marker','o');
    dot.MarkerFaceColor = color2; 
    dot.MarkerEdgeColor = color2; 
    dot.MarkerSize = markerSize;
    if(txtSize > 0)
        fitness = max(evo.generation(1).parent.fractionalConeBandWidth);
        txt = text(0,0, ['  ',num2str(round(fitness*10000)/100),'%']);
        txt.FontSize = txtSize;
    end
    
    
    
    %axis and so 
    
    d = maxx - minx;
    ax = gca;
    ax.XLim = [minx-0.05*d, maxx+0.2*d];
    axis off;
    
    
    %output
    out = [genOut,chiOut];
    
    

end

