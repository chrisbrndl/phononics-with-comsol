function run(evo,DEBUG,PRINT)
%RUN runs the evolution


    %DEBUG = true;
    %PRINT = false;
    
    
    finishup = onCleanup(@() statusInterrupt(evo));
    
    finished = false;
    evo.status = 'processing';
    save(evo.file,'evo');
    
    try

        while(~finished)

            %if the generation array is empty start from scratch
            if(isempty(evo.generation) )
                
                if(PRINT)
                    disp([char(datetime('now')), ': start new evolution']);
                end
                
                load(evo.seedGeometry);

                g.parent = S;
                g.parentIdx = 0;
                g.children = [];
                g.mutationStrength = evo.mutationStrength(1);

                evo.generation = [evo.generation, g];

            %go to the last generation     
            else

                %check out the status of the evolution
                numberOfGenerations = length(evo.generation);
                numberOfMutations = length(evo.generation(numberOfGenerations).children);
                if(PRINT)
                    disp([char(datetime('now')), ': [generation|mutation] = [' ,sprintf('%03d',numberOfGenerations),'|', sprintf('%03d',numberOfMutations),']']);
                end

                parent = evo.generation(numberOfGenerations).parent;
                mutationStrength = evo.generation(numberOfGenerations).mutationStrength;

                %check if batchsize is complete
                if( numberOfMutations ~= 0 && mod(numberOfMutations,evo.batchSize)==0 )

                    g = evo.generation(numberOfGenerations);
                    if(PRINT)
                        disp(['                      ', 'batch is complete']);
                    end
                    fitness_parent = max( parent.fractionalConeBandWidth );

                    fitness_child  = 0;
                    newParentIdx = 0;
                    for i=1:length( g.children )
                        if(  max(g.children(i).fractionalConeBandWidth) > fitness_child  ) 
                            fitness_child = max(g.children(i).fractionalConeBandWidth);
                            newParentIdx = i;
                        end
                    end

                    if(fitness_child > fitness_parent)
                        %start a new generation without changing the mutationStrength
                        if(PRINT)
                            disp(['                      ', 'better mutation was found']);
                        end
                        gNew.parent = g.children(newParentIdx);
                        gNew.parentIdx = newParentIdx;
                        gNew.children = [];
                        gNew.mutationStrength = g.mutationStrength;
                        evo.generation = [evo.generation, gNew];
                        if(PRINT)
                            disp(['                      ', '-->   generate a new generation [mutationStrength = ',num2str(gNew.mutationStrength),']']);
                        end
                    else
                        if(PRINT)
                            disp(['                      ', 'no better mutation was found']);
                        end
                        currentBatch= length(g.children)/evo.batchSize;
                        if(currentBatch < evo.maxBatches)
                            if(PRINT)
                                disp(['                      ', 'maximum batches not reached']);
                            end
                            %build, evaluate and append a mutation
                            child = parent.mutate(mutationStrength);
                            if(DEBUG)
                                fractionalBandWidth = (1+((2*(rand-0.5))-0.55)*mutationStrength) * max(parent.fractionalConeBandWidth);
                                child.evaluateStructureFake(fractionalBandWidth)
                            else
                                try
                                    child.evaluateStructureMutation();
                                catch
                                    disp([' -->  WE GOT AN ERROR BECAUSE OF STUPID COMSOL, LETS JUST SKIP IT!']);    
                                end
                            end
                            evo.generation(numberOfGenerations).children = [evo.generation(numberOfGenerations).children, child];

                            fc = max(child.fractionalConeBandWidth);
                            fp = max(parent.fractionalConeBandWidth);
                            if(fc>fp)
                                if(PRINT)
                                    disp(['                      ', '-->   generate a new structure [',num2str(fc),' > ',num2str(fp),'] * ']);
                                end
                            else
                                if(PRINT)
                                    disp(['                      ', '-->   generate a new structure [',num2str(fc),' < ',num2str(fp),']']);
                                end
                            end
                            save(evo.file,'evo')

                        else
                            if(PRINT)
                                disp(['                      ', 'reached maximum batches']);
                            end
                            
                            idx = find(evo.mutationStrength == g.mutationStrength);
                            if(idx<length(evo.mutationStrength) )

                                gNew.parent = g.parent;
                                gNew.parentIdx = [];
                                gNew.children = [];
                                gNew.mutationStrength = evo.mutationStrength(idx+1);
                                evo.generation = [evo.generation, gNew];
                                if(PRINT)
                                    disp(['                      ', '-->   generate a new generation [mutationStrength = ',num2str(gNew.mutationStrength),']']);
                                end
                            else
                                if(PRINT)
                                    disp(['                      ', 'reached minimum mutationStrength']);
                                    disp(['                      ', '-->   evolution converged!!!']);
                                end
                                finished = true;
                                evo.status = 'finished';
                                save(evo.file,'evo')
                            end

                        end
                        %if already maxBatch -> new generation with next
                        %mutationStrength
                            %if no more mutation strength -> finish

                        %else new structure
                    end


                else
                    if(PRINT)
                        disp(['                      ', 'batch is incomplete']);
                    end

                    %build, evaluate and append a mutation
                    child = parent.mutate(mutationStrength);



                    if(DEBUG)
                        fractionalBandWidth = (1+((2*(rand-0.5))-0.55)*mutationStrength) * max(parent.fractionalConeBandWidth);
                        child.evaluateStructureFake(fractionalBandWidth)
                    else
                        try
                            child.evaluateStructureMutation();
                        catch
                            disp([' -->  WE GOT AN ERROR BECAUSE OF STUPID COMSOL, LETS JUST SKIP IT!']);    
                        end
                    end

                    evo.generation(numberOfGenerations).children = [evo.generation(numberOfGenerations).children, child];

                    fc = max(child.fractionalConeBandWidth);
                    fp = max(parent.fractionalConeBandWidth);
                    if(fc>fp)
                        if(PRINT)
                            disp(['                      ', '-->   generate a new structure [',num2str(fc),' > ',num2str(fp),'] * ']);
                        end
                    else
                        if(PRINT)
                            disp(['                      ', '-->   generate a new structure [',num2str(fc),' < ',num2str(fp),']']);
                        end
                    end


                    save(evo.file,'evo')

                end


            end

        end
        
    catch exception
        
        evo.status = 'waiting';
        throw(exception);
        
    end
    
    

end


%cleanup function
function []=statusInterrupt(evo)
    if(evo.status == 'processing')
        evo.status = 'waiting';
        disp('evo.status set to WAITING');
        save(evo.file,'evo')
    end
end