function generateComsolModel(obj, mesh_size)
%generateComsolModel 
%   generates the COMSOL model
    
    % import Comsol stuff
    import com.comsol.model.*
    import com.comsol.model.util.*
    
    % create model and geometry node
    model = ModelUtil.create('Model');
    model.modelNode.create('comp1');
    geometry = model.geom.create('geom1',3);

    % set the standard length to m 
    geometry.lengthUnit('m');
    
    tic
    % workplane
    workplane = geometry.feature.create('wp1','WorkPlane');
    workplane.set('quickz','0');
    
    % square
    square = workplane.geom.create('pol1', 'Polygon');
    square.set('source', 'table');
    h = obj.lattice_constant/2;
    % x-values
    square.setIndex('table', num2str(-h), 0, 0);
    square.setIndex('table', num2str(+h), 1, 0);
    square.setIndex('table', num2str(+h), 2, 0);
    square.setIndex('table', num2str(-h), 3, 0);
    % y-values
    square.setIndex('table', num2str(-h), 0, 1);
    square.setIndex('table', num2str(-h), 1, 1);
    square.setIndex('table', num2str(+h), 2, 1);
    square.setIndex('table', num2str(+h), 3, 1);
    
    % holes
    hole_names = {};
    for i=1:length(obj.polygon_holes)
        hole_coords = obj.polygon_holes{i};
        hole_name = ['hole_',num2str(i)];
        hole_names = [hole_names, hole_name];
        polygon = workplane.geom.create(hole_name, 'Polygon');
        polygon.set('source', 'table');
        for j=1:length(hole_coords)
            polygon.setIndex('table', num2str( hole_coords(j,1) ), j-1, 0);  %value,row, coloumn
            polygon.setIndex('table', num2str( hole_coords(j,2) ), j-1, 1);  %value,row, coloumn
        end
    end
       
    % difference 
    dif = workplane.geom.create('dif1', 'Difference');
    dif.selection('input').set({'pol1'});
    dif.selection('input2').set(hole_names);
   
    % extrusion
    ext = geometry.feature.create('ext1', 'Extrude');
    ext.set('workplane', 'wp1');
    ext.selection('input').set({'wp1'});
    ext.setIndex('distance', num2str(obj.slab_thickness/2), 0);
         
    % build geometry node
    geometry.run
            
    % definintion of the material [Si]
    Silicon = model.material.create('Silicon');
    Silicon.propertyGroup('def').set('density', '2329[kg/m^3]');
    Silicon.propertyGroup.create('Enu', 'Young''s modulus and Poisson''s ratio');
    Silicon.propertyGroup('Enu').set('poissonsratio', '0.28');
    Silicon.propertyGroup('Enu').set('youngsmodulus', '170e9[Pa]');
    Silicon.selection.set([1]);
     
    %finding the boundaries
    a = obj.lattice_constant;
    d = obj.slab_thickness;
    e = 0.01*a;
    e2 = 0.1*d;
    
    coordBox  = [-a, a; +a/2-e,  +a/2+e; -a, +a]; 
    upper = mphselectbox(model,'geom1',coordBox,'boundary');
    
    coordBox  = [-a, a; -a/2-e,  -a/2+e; -a, +a]; 
    lower = mphselectbox(model,'geom1',coordBox,'boundary');
    
    coordBox  = [-a/2-e,  -a/2+e; -a, +a; -a, +a]; 
    left = mphselectbox(model,'geom1',coordBox,'boundary');
    
    coordBox  = [+a/2-e,  +a/2+e; -a, +a; -a, +a]; 
    right = mphselectbox(model,'geom1',coordBox,'boundary');
    
    coordBox  = [-a,  +a; -a, +a; -e2, +e2]; 
    bottom = mphselectbox(model,'geom1',coordBox,'boundary');
    
    coordBox  = [-a,  +a; -a, +a; d/2-e2, d/2+e2]; 
    top = mphselectbox(model,'geom1',coordBox,'boundary');

    % comsol mesh 
    mesh = model.mesh.create('mesh1', 'geom1');  
    mesh.create('ftet1', 'FreeTet');
    mesh.feature('ftet1').create('size2', 'Size');
    mesh.feature('ftet1').feature('size2').set('custom', 'on');
    mesh.feature('ftet1').feature('size2').set('hmaxactive', 'on');
    mesh.feature('ftet1').feature('size2').set('hmax', num2str(mesh_size));
    mesh.run;
    obj.mesh_size = mesh_size;
      
    % phsics
    physics = model.physics.create('solid', 'SolidMechanics', 'geom1');
    
    physics.feature.create('pc1', 'PeriodicCondition', 2);
    physics.feature('pc1').set('PeriodicType', 'Floquet');
    physics.feature('pc1').selection.set([left right]);
    
    physics.feature.create('pc2', 'PeriodicCondition', 2);
    physics.feature('pc2').set('PeriodicType', 'Floquet');
    physics.feature('pc2').selection.set([upper lower]);
   
    physics.feature.create('sym1', 'SymmetrySolid', 2);
    physics.feature('sym1').selection.set(bottom);
      
    % return
    obj.comsol_model = model;

end

