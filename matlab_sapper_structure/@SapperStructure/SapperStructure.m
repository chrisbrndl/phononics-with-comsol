classdef SapperStructure < handle
    %SAPPERSTRUCTURE 
    %   Detailed explanation goes here
    
    properties
        lattice_constant
        slab_thickness
        polygon_holes
        
        mesh_size
        central_frequency
        number_of_frequencies
        quasimomenta
        
        comsol_model
        band_structure
    end
    
    methods (Access = public)
        
        % constructor
        function obj = SapperStructure(lattice_constant, slab_thickness, polygon_holes)
            obj.lattice_constant = lattice_constant;
            obj.slab_thickness = slab_thickness;
            obj.polygon_holes = polygon_holes;
        end
       
        generateComsolModel(obj, mesh_size);
        calculateBandstructure(obj, quasimomenta, central_frequency, number_of_frequencies)
        printGeometry(obj);
        printMesh(obj);
        saveModel(obj, file_name);
       
    end
    
end

