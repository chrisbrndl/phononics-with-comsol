function calculateBandstructure( obj, quasimomenta, central_frequency, number_of_frequencies )
%calculateBandStructure
%   calculates the band structure of the model

    study = obj.comsol_model.study.create('std');
    studyEf = study.feature.create('eig', 'Eigenfrequency');
    studyEf.activate('solid', true);
    studyEf.set('neigs', num2str(number_of_frequencies));
    studyEf.set('shift', num2str(central_frequency));
    obj.number_of_frequencies = number_of_frequencies;
    obj.central_frequency = central_frequency;
 
    bands = zeros(number_of_frequencies, length(quasimomenta));
    for i=1:length(quasimomenta)
        kx = quasimomenta(i,1);
        ky = quasimomenta(i,2);
        obj.comsol_model.physics('solid').feature('floquet_e_w').set('kFloquet', {num2str(kx) num2str(ky) '0'});
        obj.comsol_model.physics('solid').feature('floquet_ne_sw').set('kFloquet', {num2str(kx) num2str(ky) '0'});
        obj.comsol_model.physics('solid').feature('floquet_nw_se').set('kFloquet', {num2str(kx) num2str(ky) '0'});
        obj.comsol_model.study('std').run;
        data = mpheval(obj.comsol_model,'freq');
        bands(:,i)=data.d1(:,1);
    end
    
    obj.band_structure = real(bands);
    obj.central_frequency = central_frequency;
    obj.number_of_frequencies = number_of_frequencies;
end

