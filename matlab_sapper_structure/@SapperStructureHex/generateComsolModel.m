function generateComsolModel(obj, mesh_size)
%generateComsolModel 
%   generates the COMSOL model
    
    % import Comsol stuff
    import com.comsol.model.*
    import com.comsol.model.util.*
    
    % create model and geometry node
    model = ModelUtil.create('Model');
    model.modelNode.create('comp1');
    geometry = model.geom.create('geom1',3);

    % set the standard length to m 
    geometry.lengthUnit('m');
    
    tic
    % workplane
    workplane = geometry.feature.create('wp1','WorkPlane');
    workplane.set('quickz','0');
    
    % hexagon
    a = obj.lattice_constant;
    hexagon_coords = [ a/2,         0,         -a/2        ,-a/2,         0,          a/2;...          %x-values
            a/2/sqrt(3), a/sqrt(3), a/2/sqrt(3) ,-a/2/sqrt(3), -a/sqrt(3), -a/2/sqrt(3)];   %y-values
    
    hexagon = workplane.geom.create('pol1', 'Polygon');
    hexagon.set('source', 'table');
    for i=1:size(hexagon_coords,2)
        hexagon.setIndex('table', num2str( hexagon_coords(1,i) ), i-1, 0);  %value,row, coloumn
        hexagon.setIndex('table', num2str( hexagon_coords(2,i) ), i-1, 1);
    end
   
    % holes
    hole_names = {};
    for i=1:length(obj.polygon_holes)
        hole_coords = obj.polygon_holes{i};
        hole_name = ['hole_',num2str(i)];
        hole_names = [hole_names, hole_name];
        polygon = workplane.geom.create(hole_name, 'Polygon');
        polygon.set('source', 'table');
        for j=1:length(hole_coords)
            polygon.setIndex('table', num2str( hole_coords(j,1) ), j-1, 0);  %value,row, coloumn
            polygon.setIndex('table', num2str( hole_coords(j,2) ), j-1, 1);  %value,row, coloumn
        end
    end
       
    % difference 
    dif = workplane.geom.create('dif1', 'Difference');
    dif.selection('input').set({'pol1'});
    dif.selection('input2').set(hole_names);
   
    % extrusion
    ext = geometry.feature.create('ext1', 'Extrude');
    ext.set('workplane', 'wp1');
    ext.selection('input').set({'wp1'});
    ext.setIndex('distance', num2str(obj.slab_thickness/2), 0);
         
    % build geometry node
    geometry.run
            
    % definintion of the material [Si]
    Silicon = model.material.create('Silicon');
    Silicon.propertyGroup('def').set('density', '2329[kg/m^3]');
    Silicon.propertyGroup.create('Enu', 'Young''s modulus and Poisson''s ratio');
    Silicon.propertyGroup('Enu').set('poissonsratio', '0.28');
    Silicon.propertyGroup('Enu').set('youngsmodulus', '170e9[Pa]');
    Silicon.selection.set([1]);
     
    
    % phsics
    physics = model.physics.create('solid', 'SolidMechanics', 'geom1');
    
    physics.feature.create('floquet_e_w', 'PeriodicCondition', 2);
    physics.feature('floquet_e_w').set('PeriodicType', 'Floquet');
    
    physics.feature.create('floquet_ne_sw', 'PeriodicCondition', 2);
    physics.feature('floquet_ne_sw').set('PeriodicType', 'Floquet');

    physics.feature.create('floquet_nw_se', 'PeriodicCondition', 2);
    physics.feature('floquet_nw_se').set('PeriodicType', 'Floquet');
   
    physics.feature.create('sym1', 'SymmetrySolid', 2);
    
    % finding the boundaries for bottom
    a = obj.lattice_constant;
    d = obj.slab_thickness;     
    coordBox  = [-a,  +a; -a, +a; -0.1*d, +0.1*d]; 
    bottom = mphselectbox(model,'geom1',coordBox,'boundary');
    physics.feature('sym1').selection.set(bottom);
    
    % bounding boxes of left and right boundaries
    xHex = hexagon_coords(1,:);
    coordBox_right_si = [xHex(1)-1e-9,xHex(1)+1e-9; -3*a, +3*a; -1e-9, d/2+1e-9];
    coordBox_left_si = [xHex(3)-1e-9,xHex(3)+1e-9; -3*a, +3*a; -1e-9, d/2+1e-9];

    % east & west (no rotation needed)
    boundary_right_si = mphselectbox(model,'geom1',coordBox_right_si,'boundary');
    boundary_left_si = mphselectbox(model,'geom1',coordBox_left_si,'boundary');
    physics.feature('floquet_e_w').selection.set([boundary_right_si, boundary_left_si]);
    
    % northeast & southwest 
    model.geom('geom1').create('rot1', 'Rotate');
    model.geom('geom1').feature('rot1').selection('input').set({'ext1'});
    model.geom('geom1').feature('rot1').set('rot', '-60');
    model.geom('geom1').runPre('fin');

    boundary_right_si = mphselectbox(model,'geom1',coordBox_right_si,'boundary');
    boundary_left_si = mphselectbox(model,'geom1',coordBox_left_si,'boundary');

    physics.feature('floquet_ne_sw').selection.set([boundary_right_si, boundary_left_si]);

    % northwest & southeast
    model.geom('geom1').create('rot2', 'Rotate');
    model.geom('geom1').feature('rot2').selection('input').set({'rot1'});
    model.geom('geom1').feature('rot2').set('rot', '-60');
    model.geom('geom1').runPre('fin');

    boundary_right_si = mphselectbox(model,'geom1',coordBox_right_si,'boundary');
    boundary_left_si = mphselectbox(model,'geom1',coordBox_left_si,'boundary');

    physics.feature('floquet_nw_se').selection.set([boundary_right_si, boundary_left_si]);
    
    % rotate back
    model.geom('geom1').create('rot3', 'Rotate');
    model.geom('geom1').feature('rot3').selection('input').set({'rot2'});
    model.geom('geom1').feature('rot3').set('rot', '120');
    model.geom('geom1').runPre('fin');
    
    
    % comsol mesh 
    mesh = model.mesh.create('mesh1', 'geom1');  
    mesh.create('ftet1', 'FreeTet');
    mesh.feature('ftet1').create('size2', 'Size');
    mesh.feature('ftet1').feature('size2').set('custom', 'on');
    mesh.feature('ftet1').feature('size2').set('hmaxactive', 'on');
    mesh.feature('ftet1').feature('size2').set('hmax', num2str(mesh_size));
    mesh.run;
    obj.mesh_size = mesh_size;
    
    % return
    obj.comsol_model = model;

end

