clear all;
clc

% Define the geometry parameters
lattice_constant = 500e-9;
slab_thickness = 220e-9;

potentials = load("potentials")
polygon_holes={}
for index=1:  size(potentials.x{2},2)
   polygon_holes{index}=potentials.x{2}{index}*1e-9
end
disp(polygon_holes)


% Create an object of the SapperStructure-Class
S = SapperStructure(lattice_constant, slab_thickness, polygon_holes);


% Generate the comsol model
mesh_size = 50e-9;
S.generateComsolModel(mesh_size);

% print the geometry in the current frame
figure(1)
clf
S.printGeometry()





file_name = 'TestSapper.mph';
S.saveModel(file_name)




% print the mesh in the current frame
figure(2)
clf
S.printMesh()

%calculate high symmetry points
G = [0;0];
X = pi/lattice_constant * [1;0];
M = pi/lattice_constant * [1;1];
    
%calculate the quasimomenta
kRes = 4;
kx1 = linspace(G(1),X(1),kRes);
kx2 = linspace(X(1),M(1),kRes);
kx3 = linspace(M(1),G(1),kRes);
    
ky1 = linspace(G(2),X(2),kRes);
ky2 = linspace(X(2),M(2),kRes);
ky3 = linspace(M(2),G(2),kRes);
    
kx = [kx1 kx2(2:end) kx3(2:end)];
ky = [ky1 ky2(2:end) ky3(2:end)];

quasimomenta = [transpose(kx), transpose(ky)];

% calculate the bandstructure along the path
central_frequency = 0;
number_of_frequencies = 10;
S.calculateBandstructure(quasimomenta, central_frequency, number_of_frequencies )

% plot the bandstructrue in the current frame
figure(3)
clf;
% S.plotBandStructure()

% save the modal as mph file to view in COMSOL Desktop
file_name = 'TestSapper.mph';
S.saveModel(file_name)